<?php
/**
* Plugin Name: Universal Shortcodes
* Plugin URI: http://themeforest.net/user/DankovThemes
* Description: Visual Composer Extantion.
* Version: 1.0.8
* Author: DankovThemes
* Author URI: http://themeforest.net/user/DankovThemes
* License:
*Text Domain: universal-wp 
*/
/* ------------------------------------------------------------------------ */
/* Plugin Scripts */
/* ------------------------------------------------------------------------ */

add_action( 'init', 'universal_shortcodes_load_textdomain' );

function universal_shortcodes_load_textdomain() {
  load_plugin_textdomain( 'universal-wp', false, basename( dirname( __FILE__ ) ) . '/languages' ); 
}

add_action('wp_enqueue_scripts', 'universal_shortcodes_scripts');

if ( !function_exists( 'universal_shortcodes_scripts' ) ) {
	function universal_shortcodes_scripts() {
		wp_enqueue_script('universal_vc_custom',plugin_dir_url( __FILE__ ).'vc_extend/vc_custom.js', false, null , true);
		wp_enqueue_script('universal_classie',plugin_dir_url( __FILE__ ).'vc_extend/classie.js', false, null , true);
		wp_enqueue_script('universal_rotator',plugin_dir_url( __FILE__ ).'vc_extend/text-rotator.min.js', false, null , true);
		wp_enqueue_script('universal_ytplayer',plugin_dir_url( __FILE__ ).'vc_extend/jquery.mb.YTPlayer.min.js', false, null , true);
		wp_enqueue_script('universal_vimeo_player',plugin_dir_url( __FILE__ ).'vc_extend/jquery.mb.vimeo_player.min.js', false, null , true);
		wp_enqueue_script('universal_vegas',plugin_dir_url( __FILE__ ).'vc_extend/vegas.min.js', false, null , true);
		wp_enqueue_script('universal_circle',plugin_dir_url( __FILE__ ).'vc_extend/jquery.circle-progress.min.js', false, null , true);
	}    
};




function universal_enqueue_sstyle() {
    wp_enqueue_style( 'universal_vegas', plugin_dir_url( __FILE__ ) . 'vc_extend/vegas.min.css', array(), '1', 'all' );
    wp_enqueue_style( 'universal_vc_style', plugin_dir_url( __FILE__ ) . 'vc_extend/vc.css', array(), '1', 'all' );
}
add_action( 'wp_enqueue_scripts', 'universal_enqueue_sstyle' );

	include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
	if ( is_plugin_active( 'js_composer/js_composer.php' ) ) {
		include( plugin_dir_path( __FILE__ ).'vc_extend/vc.php'); //Visual Composer
}