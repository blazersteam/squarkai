<?php 
/*Button*/
add_shortcode('universal_vc_button', 'universal_vc_button_f');
function universal_vc_button_f( $atts, $content = null)
{
	
	extract(shortcode_atts(
		array(
			'universal_title' => 'Try it now!',
			'universal_title_size'=>'16px',
			'universal_title_color'=>'#fff',
			'universal_bg_color'=>'#5AC8FB',
			'universal_url' => '#',
			'universal_display' => 'block',
			'universal_target'=>'_self',
			'universal_padding'=>'7px 15px',
			'universal_margin'=>'10px 0 0 0',
			'universal_border_w' => '1px',
			'universal_border_s' => 'solid',
			'universal_border_c' => '#5AC8FB',
			'universal_border_r' => '4px',
			'universal_title_color_hover'=>'#555',
			'universal_bg_color_hover'=>'#fff',
			'universal_border_c_hover' => '#555',

		), $atts)
	);
	$content = '';

	$content .='<a class="universal_vc_button" data-title-color-hover="'.$universal_title_color_hover.'" data-bg-color-hover="'.$universal_bg_color_hover.'" data-border-c-hover="'.$universal_border_c_hover.'"  href="'.$universal_url.'" target="'.$universal_target.'" style="display:'.$universal_display.'; font-size:'.$universal_title_size.'; line-heigth:'.$universal_title_size.'; color:'.$universal_title_color.'; background-color:'.$universal_bg_color.'; padding:'.$universal_padding.'; margin:'.$universal_margin.'; border-width:'.$universal_border_w.'; border-style:'.$universal_border_s.'; border-color:'.$universal_border_c.'; border-radius:'.$universal_border_r.'">'.$universal_title.'';

	$content .='</a>';
	return $content;
};


/*Button*/
vc_map( array(
	"name" => __("Button",'universal-wp'),
	"base" => "universal_vc_button",
	"category" => __('Universal','universal-wp'),
	"params" => array(
		array(
			"type" => "textfield",
			"param_name" => "universal_url",
			"heading" => __("URL", 'universal-wp'),
			"value" => '#',
			"group" => "General",
			"admin_label" => true,
		),
		array(
			'type' => 'dropdown',
			'heading' => "Target",
			'param_name' => 'universal_target',
			'value' => array( "_blank", "_self" ),
			'std' => '_self',
			"admin_label" => true,
			"group" => "General"
		),
		array(
			'type' => 'dropdown',
			'heading' => "Display",
			'param_name' => 'universal_display',
			'value' => array( "block", "inline-block" ),
			'std' => 'block',
			"group" => "General"
		),
		array(
			"type" => "textfield",
			"param_name" => "universal_padding",
			"heading" => __("Padding", 'universal-wp'),
			"value" => '10px 20px',
			"group" => "General"
		),
		array(
			"type" => "textfield",
			"param_name" => "universal_margin",
			"heading" => __("Margin", 'universal-wp'),
			"value" => '0px',
			"group" => "General"
		),
		array(
			"type" => "textfield",
			"param_name" => "universal_title",
			"heading" => __("Title", 'universal-wp'),
			"value" => 'Button',
			"admin_label" => true,
			"group" => "Title"
		),
		array(
			"type" => "textfield",
			"param_name" => "universal_title_size",
			"heading" => __("Font Size", 'universal-wp'),
			"value" => '16px',
			"group" => "Title"
		),
		array(
			"type" => "colorpicker",
			"param_name" => "universal_title_color",
			"heading" => __("Title Color", 'universal-wp'),
			"value" => '#fff',
			"group" => "Title"
		),
		array(
			"type" => "colorpicker",
			"param_name" => "universal_title_color_hover",
			"heading" => __("Hover Title Color", 'universal-wp'),
			"value" => '#fff',
			"group" => "Title"
		),
		
		array(
			"type" => "colorpicker",
			"param_name" => "universal_bg_color",
			"heading" => __("Background Color", 'universal-wp'),
			"value" => '#000',
			"group" => "Background"
		),
		array(
			"type" => "colorpicker",
			"param_name" => "universal_bg_color_hover",
			"heading" => __("HOVER Background Color", 'universal-wp'),
			"value" => '#00f6ff',
			"group" => "Background"
		),
		array(
			"type" => "textfield",
			"param_name" => "universal_border_w",
			"heading" => __("Border Width", 'universal-wp'),
			"value" => '1px',
			"group" => "Border"
		),
		array(
			"type" => "colorpicker",
			"param_name" => "universal_border_c",
			"heading" => __("Border Color", 'universal-wp'),
			"value" => '#000',
			"group" => "Border"
		),
		array(
			"type" => "colorpicker",
			"param_name" => "universal_border_c_hover",
			"heading" => __("HOVER Border Color", 'universal-wp'),
			"value" => '#00f6ff',
			"group" => "Border"
		),
		array(
			'type' => 'dropdown',
			'heading' => "Border Style",
			'param_name' => 'universal_border_s',
			'value' => array( "solid", "dotted", "dashed"),
			"group" => "Border"
		),
		
		array(
			"type" => "textfield",
			"param_name" => "universal_border_r",
			"heading" => __("Border radius", 'universal-wp'),
			"value" => '3px',
			"group" => "Border"
		),
	)
) );