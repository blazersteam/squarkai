<?php 
/*Services*/
add_shortcode('universal_circle', 'universal_circle_f');
function universal_circle_f( $atts, $content = null)
{

	extract(shortcode_atts(
		array(
			'universal_title' => 'Marketing',
			'universal_think' => '5',
			'universal_value' => '0.85',
			'universal_color' => '#333',
			'universal_color_empty' => '#C7C7C7',
			'white' => null,
			"css" => null,
		), $atts)
	);

	if ($white) $white = 'white';

	$output ='<div class="progress-circle '. esc_attr($white) .'">
				<div data-thickness="'. esc_attr($universal_think) .'" data-value="'. esc_attr($universal_value) .'" class="circle"><span></span></div>
              	<div class="agenda">'. esc_attr($universal_title) .'</div>
            </div>';
	$output .='
		<script>
(function($){
    "use strict";
    $(document).ready(function() {

		        var el = $(".circle");
		        var inited = false;

		        el.appear({
		            force_process: true
		        });
		        el.on("appear", function() {
		            if (!inited) {
		                el.circleProgress();
		                inited = true;
		            }
		        });


		        $(".circle").circleProgress({
		            size: 100,
		            fill: {
		                color: "'.$universal_color.'"
		            },
		            emptyFill: "'.$universal_color_empty.'",
		            startAngle: 300,
		            animation: {
		                duration: 4000
		            }
		        }).on("circle-animation-progress", function (event, progress, stepValue) {
		            $(this).find("span").text((stepValue * 100).toFixed(1));
		        });
		
    });
		})(jQuery);

		</script>';
	return $output;

};

/*Circle*/
vc_map( array(
	"name" => __("Animated Circle",'universal-wp'),
	"base" => "universal_circle",
	"category" => __('Universal','universal-wp'),
	"params" => array(
		array(
			"type" => "textfield",
			"admin_label" => true,
			"param_name" => "universal_title",
			"heading" => __("Icon", 'universal-wp'),
			"value" => 'Marketing',
		),
		array(
			"type" => "textfield",
			"admin_label" => true,
			"param_name" => "universal_think",
			"heading" => __("Level of thinkness", 'universal-wp'),
			"value" => '5',
		),
		array(
			"type" => "textfield",
			"admin_label" => true,
			"param_name" => "universal_value",
			"heading" => __("Value", 'universal-wp'),
			"value" => '0.85',
			"description" => __( 'From 0.1 to 1', 'universal-wp' ),
		),
		array(
			"type" => "colorpicker",
			"admin_label" => true,
			"param_name" => "universal_color",
			"heading" => __("Main Color", 'universal-wp'),
            "value" => '#333', 
		),
		array(
			"type" => "colorpicker",
			"admin_label" => true,
			"param_name" => "universal_color_empty",
			"heading" => __("Empty Fill Color", 'universal-wp'),
            "value" => '#C7C7C7', 
		),
        array(
			"type" => "checkbox",
			"admin_label" => true,
			"heading" => __("White fonts", 'universal-wp'),
			"param_name" => "white",
			"value" => array("Yes" => true),
		),
	)
) );