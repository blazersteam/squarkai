<?php
add_shortcode('vc_cooming_soon_three', 'vc_cooming_soon_three_f');
function vc_cooming_soon_three_f( $atts, $content = null)
{
	extract(shortcode_atts(
		array(
			'universal_image' => null,
			'title' => 'universal, coming soon, really soon, stay tuned',
			'placeholder' => 'E-mail address...',
			'button_title' => 'GET NOTIFIED!',
			'time' => '2018/01/21 11:00:00',
			'mailchimp' => 'http://forbetterweb.us11.list-manage.com/subscribe/post?u=4f751a6c58b225179404715f0&amp;id=18fc72763a',
		), $atts)
	);

    $image = wp_get_attachment_image_src($universal_image, true);
    $image = $image[0];

		$output ='
		<div style="background: url('. esc_url($image) .'); background-position: 50% 50%;" class="intro full-coming">
		      <div class="intro-body">
		        <div class="container">
		          <div class="row">
            		<div class="col-sm-8 col-sm-offset-2">
		              <h1 class="no-pad"><span class="rotate">'.$title.'</span></h1>
              		<div id="clock" class="no-pad-top"></div>
              <form id="mc-embedded-subscribe-form2" action="'. esc_url($mailchimp) .'" method="post" name="mc-embedded-subscribe-form" target="_blank" novalidate="" class="small-form subscribe-form">
                <div class="input-group input-group-lg">
                  <input id="mce-EMAIL2" type="email" name="EMAIL" placeholder="'.$placeholder.'" class="form-control"><span class="input-group-btn">
                    <button id="mc-embedded-subscribe2" type="submit" name="subscribe" class="btn btn-dark">'.$button_title.'</button></span>
                </div>
                <div id="mce-responses2">
                  <div id="mce-error-response2" style="display:none" class="response"></div>
                  <div id="mce-success-response2" style="display:none" class="response"></div>
                </div>
              </form>
		        <ul class="list-inline">';
		                  	if(get_theme_mod('universal_fot_soc_twitter','enable') == true) {
		                  		$output .='<li><a href="'.esc_url(stripslashes(get_theme_mod("universal_fot_soc_twitter","http://twitter.com"))) .'"><i class="fa fa-twitter fa-fw fa-2x"></i></a></li>';
		                  	};
		                  	if(get_theme_mod('universal_fot_soc_facebook','enable') == true) {
		                  	 	$output .='<li><a href="'.esc_url(stripslashes(get_theme_mod("universal_fot_soc_facebook","http://facebook.com"))) .'"><i class="fa fa-facebook fa-fw fa-2x"></i></a></li>';
		                  	}; 
		                  	if(get_theme_mod('universal_fot_soc_googleplus','enable') == true) {
		                  	 	$output .='<li><a href="'.esc_url(stripslashes(get_theme_mod("universal_fot_soc_googleplus","http://plus.google.com"))) .'"><i class="fa fa-google-plus fa-fw fa-2x"></i></a></li>';
		                  	}; 
		                  	if(get_theme_mod('universal_fot_soc_linkedin','enable') == true) {
		                  	 	$output .='<li><a href="'.esc_url(stripslashes(get_theme_mod("universal_fot_soc_linkedin","http://linkedin.com"))) .'"><i class="fa fa-linkedin fa-fw fa-2x"></i></a></li>';
		                  	}; 
		                  	if(get_theme_mod('universal_fot_soc_instagram') == true) {
		                  	 	$output .='<li><a href="'.esc_url(stripslashes(get_theme_mod("universal_fot_soc_instagram"))) .'"><i class="fa fa-instagram fa-fw fa-2x"></i></a></li>';
		                  	}; 
							if(get_theme_mod('universal_fot_soc_youtube') == true) {
							 	$output .='<li><a href="'.esc_url(stripslashes(get_theme_mod("universal_fot_soc_youtube"))) .'"><i class="fa fa-youtube-play fa-fw fa-2x"></i></a></li>';
							};   
							if(get_theme_mod('universal_fot_soc_flickr') == true) {
							 	$output .='<li><a href="'.esc_url(stripslashes(get_theme_mod("universal_fot_soc_flickr"))) .'"><i class="fa fa-flickr fa-fw fa-2x"></i></a></li>';
							};   
							if(get_theme_mod('universal_fot_soc_tumblr') == true) {
							 	$output .='<li><a href="'.esc_url(stripslashes(get_theme_mod("universal_fot_soc_tumblr"))) .'"><i class="fa fa-tumblr fa-fw fa-2x"></i></a></li>';
							};   
							if(get_theme_mod('universal_fot_soc_foursquare') == true) {
							 	$output .='<li><a href="'.esc_url(stripslashes(get_theme_mod("universal_fot_soc_foursquare"))) .'"><i class="fa fa-foursquare fa-fw fa-2x"></i></a></li>';
							};   
							if(get_theme_mod('universal_fot_soc_vk') == true) {
							 	$output .='<li><a href="'.esc_url(stripslashes(get_theme_mod("universal_fot_soc_vk"))) .'"><i class="fa fa-vk fa-fw fa-2x"></i></a></li>';
							};   
							if(get_theme_mod('universal_fot_soc_behance') == true) {
							 	$output .='<li><a href="'.esc_url(stripslashes(get_theme_mod("universal_fot_soc_behance"))) .'"><i class="fa fa-behance fa-fw fa-2x"></i></a></li>';
							};   
							if(get_theme_mod('universal_fot_soc_pinterest') == true) {
							 $output .='<li><a href="'.esc_url(stripslashes(get_theme_mod("universal_fot_soc_pinterest"))) .'"><i class="fa fa-pinterest fa-fw fa-2x"></i></a></li>';
							};
							if(get_theme_mod('universal_fot_soc_github') == true) {
							 	$output .='<li><a href="'.esc_url(stripslashes(get_theme_mod("universal_fot_soc_github"))) .'"><i class="fa fa-github fa-fw fa-2x"></i></a></li>';
							};   
							if(get_theme_mod('universal_fot_soc_rss') == true) {
							 	$output .='<li><a href="'.esc_url(stripslashes(get_theme_mod("universal_fot_soc_rss"))) .'"><i class="fa fa-rss fa-fw fa-2x"></i></a></li>';
							};
		            $output .='</ul>
		              <p class="footer-copy-text small">'. get_theme_mod( "universal_footer_love", "We <i class='fa fa-heart fa-fw'></i> creative people" ) .'</p>
		            </div>
		          </div>
		        </div>
		      </div>
		    </div>';
		    $output .='<script>
				jQuery.noConflict()(function($){
				"use strict";
			        $("#clock").countdown("'.$time.'").on("update.countdown", function (event) {
			            var $this = $(this).html(event.strftime(""
			                + "<div><span>%-w</span>week%!w</div>"
			                + "<div><span>%-d</span>day%!d</div>"
			                + "<div><span>%H</span>hr</div>"
			                + "<div><span>%M</span>min</div>"
			                + "<div><span>%S</span>sec</div>"));
			        });
				});
				</script>';
	
	return $output;
};

vc_map( array(
	"name" => __("Cooming Soon #3",'universal-wp'),
	"base" => "vc_cooming_soon_three",
	"category" => __('Universal','universal-wp'),
	"params" => array(
	    array(
			"type" => "attach_image",
			"param_name" => "universal_image",
			"heading" => __("Image", 'universal-wp'),
			"admin_label" => true,
	    ),	
		array(
			"type" => "textfield",
			"admin_label" => true,
			"param_name" => "title",
			"heading" => __("Title", 'universal-wp'),
			"value" => 'universal, coming soon, really soon, stay tuned',
		),
		array(
			"type" => "textfield",
			"admin_label" => true,
			"param_name" => "button_title",
			"heading" => __("Button Text", 'universal-wp'),
			"value" => 'GET NOTIFIED!',
		),
		array(
			"type" => "textfield",
			"admin_label" => true,
			"param_name" => "placeholder",
			"heading" => __("Placeholder", 'universal-wp'),
			"value" => 'E-mail address...',
		),	
		array(
			"type" => "textfield",
			"admin_label" => true,
			"param_name" => "time",
			"heading" => __("End Time", 'universal-wp'),
			"value" => '2018/01/21 11:00:00',
		),
		array(
			"type" => "textfield",
			"admin_label" => true,
			"param_name" => "mailchimp",
			"heading" => __("MailChimp Action", 'universal-wp'),
			"value" => 'http://forbetterweb.us11.list-manage.com/subscribe/post?u=4f751a6c58b225179404715f0&amp;id=18fc72763a',
		),	
	)
) );