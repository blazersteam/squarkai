<?php 
/*Contacts Us*/
add_shortcode('universal_contacts_us', 'universal_contacts_us_f');
function universal_contacts_us_f( $atts, $content = null)
{

	extract(shortcode_atts(
		array(
			'universal_contacts_us_text' => 'Feel free to contact us. A business has to be involving, it has to be fun, and it has to exercise your creative instincts. Start where you are. Use what you have. Do what you can. Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
			'universal_contacts_us_address' => '1234 Some Avenue, New York, NY 56789',
			'universal_contacts_us_email' => 'info@youwebsite.com',
			'universal_contacts_us_phone' => '(123) 456-7890',
			"css" => null
		), $atts)
	);

	if ($universal_contacts_us_text!=false){ $output ='<p>'. esc_attr($universal_contacts_us_text) .'</p><hr>';};
	if ($universal_contacts_us_address!=false){	$output .='<h5><i class="fa fa-map-marker fa-fw fa-lg"></i> '. esc_attr($universal_contacts_us_address) .'</h5>';};
	if ($universal_contacts_us_email!=false){	$output .='<h5><i class="fa fa-envelope fa-fw fa-lg"></i> <a href="mailto:'. esc_attr($universal_contacts_us_email) .'">'. esc_attr($universal_contacts_us_email) .'</a></h5>';};
	if ($universal_contacts_us_phone!=false){	$output .='<h5><i class="fa fa-phone fa-fw fa-lg"></i> <a href="tel:'. esc_attr($universal_contacts_us_phone) .'">'. esc_attr($universal_contacts_us_phone) .'</a></h5>';};
return $output;

};


vc_map( array(
	"name" => __("Contacts Us",'universal-wp'),
	"base" => "universal_contacts_us",
	"category" => __('Universal','universal-wp'),
	"params" => array(
		array(
			"type" => "textarea",
			"admin_label" => true,
			"param_name" => "universal_contacts_us_text",
			"heading" => __("Text", 'universal-wp'),
			"value" => 'Feel free to contact us. A business has to be involving, it has to be fun, and it has to exercise your creative instincts. Start where you are. Use what you have. Do what you can. Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
		),
		array(
			"type" => "textfield",
			"admin_label" => true,
			"param_name" => "universal_contacts_us_address",
			"heading" => __("Address", 'universal-wp'),
			"value" => '1234 Some Avenue, New York, NY 56789',
		),
		array(
			"type" => "textfield",
			"admin_label" => true,
			"param_name" => "universal_contacts_us_email",
			"heading" => __("E-mail", 'universal-wp'),
			"value" => 'info@youwebsite.com',
		),
		array(
			"type" => "textfield",
			"admin_label" => true,
			"param_name" => "universal_contacts_us_phone",
			"heading" => __("Phone", 'universal-wp'),
			"value" => '(123) 456-7890',
		),
	)
) );