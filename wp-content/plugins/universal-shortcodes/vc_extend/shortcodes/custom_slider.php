<?php
/*Custom Slider*/
add_shortcode('universal_custom_slider', 'universal_custom_slider_f');
function universal_custom_slider_f( $atts, $content = null)
{
	extract(shortcode_atts(
		array(
			'universal_id' => 'example_id',
			'universal_slider_dots_nav' =>'Show',
			'universal_descr' => "Show",
			'universal_autoplay' =>'true',
			'universal_margin' =>'0',
			'universal_time' =>'5000',
			'universal_items_1400' =>'1',
			'universal_items_m_1400' =>'0',
			'universal_items_1200' =>'1',
			'universal_items_m_1200' =>'0',
			'universal_items_800' =>'1',
			'universal_items_m_800' =>'0',
			'universal_items_600' =>'1',
			'universal_items_m_600' =>'0',
			'universal_items_0' =>'1',
			'universal_items_m_0' =>'0',
			'wow' => null,
			'wow_delay' => '0.1',
			'wow_animate' => 'fadeIn',
			'css_class' => 'margin: 0;',
		), $atts)
	);



	if ($wow) $wow = 'wow';

	$universal_dots_nav ='true';
	$output = '';

	if($universal_slider_dots_nav =="Dots"){
		$universal_dots_nav ='true';
	}


	$extra_class='';
	if($universal_descr == 'None'){$extra_class ='do_not_show_hover';}

	
	$output .='<div class="'.esc_attr($wow).' '. esc_attr($wow_animate) .'" data-wow-delay="'. esc_attr($wow_delay) .'s"><div style="'. esc_attr($css_class).'" class="owl-carousel '.$extra_class.'" data-dots="'.$universal_dots_nav.'"  class="universal_owl_slider" id="'.$universal_id.'">'.do_shortcode($content).'</div></div>';


	$output .='<script>
jQuery(window).load(function(){
		jQuery("#'.$universal_id.'").owlCarousel({
			loop:true,
			autoplay:'.$universal_autoplay.',
			margin:'.$universal_margin.',
			dots:'.$universal_dots_nav.',
			autoHeight: true,
			animateIn: "fadeIn",
			animateOut: "fadeOut",
			autoplayTimeout:'.$universal_time.',
			navText:[,],
			responsive: {
				0: {
					margin: '.$universal_items_m_0.',
					items: '.$universal_items_0.'
				},
				600: {
					margin: '.$universal_items_m_600.',
					items: '.$universal_items_600.'
				},
				800: {
					margin: '.$universal_items_m_800.',
					items: '.$universal_items_800.'
				},
				1200: {
					margin: '.$universal_items_m_1200.',
					items: '.$universal_items_1200.'
				},
				1400: {
					margin: '.$universal_items_m_1400.',
					items: '.$universal_items_1400.'
				}
			}
		});
		});
	</script>';

	return $output;
};

/*Custom Slider*/
vc_map( array(
    "name" => __("Custom Slider", 'universal-wp'),
    "base" => "universal_custom_slider",
	"category" => __('Universal','universal-wp'),
    "as_parent" => array('only' => 'vc_testimonial_item, vc_portfolio_item, vc_image_item'), // Use only|except attributes to limit child shortcodes (separate multiple values with comma)
    "content_element" => true,
    "show_settings_on_create" => true,
    "params" => array(
		array(
			"type" => "textfield",
			"admin_label" => true,
			"param_name" => "universal_id",
			"group" => "General",
			"heading" => __("Slider ID", 'universal-wp'),
			"value" => 'example_id',
			"description" => __( "Please set slider ID", 'universal-wp' )
		),
		array(
			'type' => 'dropdown',
			'heading' => "Autoplay",
			'param_name' => 'universal_autoplay',
			"group" => "General",
			'value' => array( "true", "false"),
			'std' => 'true',
			"admin_label" => true,
		),
		array(
			"type" => "textfield",
			"admin_label" => true,
			"param_name" => "universal_time",
			"group" => "General",
			"heading" => __("Autoplay Timeout", 'universal-wp'),
			"value" => '5000',
		),

		array(
			'type' => 'dropdown',
			'heading' => "Navigation Dots",
			'param_name' => 'universal_slider_dots_nav',
			"group" => "General",
			'value' => array( "Show", "Hide"),
			'std' => 'Show',
			"admin_label" => true,
		),

		array(
			"type" => "textfield",
			"param_name" => "universal_margin",
			"group" => "General",
			"heading" => __("Space Between Slide", 'universal-wp'),
			"description" => __( "only one number. Ex: 10.", 'universal-wp' ),
			"value" => '0',
		),
		array(
			"type" => "textfield",
			"param_name" => "css_class",
			"group" => "General",
			"heading" => __("Margin", 'universal-wp'),
			"value" => 'margin: 0px',
		),
		array(
			"type" => "textfield",
			"group" => "Responsive",
			"param_name" => "universal_items_1400",
			"heading" => __("Items per row for 1400px wide screen", 'universal-wp'),
			"value" => '1',
			"description" => __( "For big desktops", 'universal-wp' )
		),
		array(
			"type" => "textfield",
			"group" => "Responsive",
			"param_name" => "universal_items_m_1400",
			"heading" => __("Space between for items 1400px wide screen", 'universal-wp'),
			"value" => '0',
			"description" => __( "For big desktops", 'universal-wp' )
		),
		
		array(
			"type" => "textfield",
			"group" => "Responsive",
			"param_name" => "universal_items_1200",
			"heading" => __("Items per row for 1200px wide screen", 'universal-wp'),
			"value" => '1',
			"description" => __( "For standard desktops", 'universal-wp' )
		),
		array(
			"type" => "textfield",
			"group" => "Responsive",
			"param_name" => "universal_items_m_1200",
			"heading" => __("Space between for items 1200px wide screen", 'universal-wp'),
			"value" => '0',
			"description" => __( "For standard desktops", 'universal-wp' )
		),
		
		array(
			"type" => "textfield",
			"group" => "Responsive",
			"param_name" => "universal_items_800",
			"heading" => __("Items per row for 800px wide screen", 'universal-wp'),
			"value" => '1',
			"description" => __( "For landscape tablet view", 'universal-wp' )
		),
		array(
			"type" => "textfield",
			"group" => "Responsive",
			"param_name" => "universal_items_m_800",
			"heading" => __("Space between items for 800px wide screen", 'universal-wp'),
			"value" => '0',
			"description" => __( "For landscape tablet view", 'universal-wp' )
		),
		
		array(
			"type" => "textfield",
			"group" => "Responsive",
			"param_name" => "universal_items_600",
			"heading" => __("Items per row for 600px wide screen", 'universal-wp'),
			"value" => '1',
			"description" => __( "For portrait tablet view", 'universal-wp' )
		),
		array(
			"type" => "textfield",
			"group" => "Responsive",
			"param_name" => "universal_items_m_600",
			"heading" => __("Space between items for 600px wide screen", 'universal-wp'),
			"value" => '0',
			"description" => __( "For portrait tablet view", 'universal-wp' )
		),
		
		array(
			"type" => "textfield",
			"group" => "Responsive",
			"param_name" => "universal_items_0",
			"heading" => __("Items per row for mobile", 'universal-wp'),
			"value" => '1',
			"description" => __( "For mobile", 'universal-wp' )
		),
		array(
			"type" => "textfield",
			"group" => "Responsive",
			"param_name" => "universal_items_m_0",
			"heading" => __("Space between items for mobile", 'universal-wp'),
			"value" => '0',
			"description" => __( "For mobile", 'universal-wp' )
		),
		array(
			"type" => "checkbox",
			"heading" => __("Animate", 'universal-wp'),
			"param_name" => "wow",
			"value" => array("Yes" => true),
            "group" => __("Animate", 'universal-wp'),
		),
		array(
			"type" => "textfield",
			"heading" => __("Delay", 'universal-wp'),
			"param_name" => "wow_delay",
			"value" => '100',
			"description" => 'in s',
            "group" => __("Animate", 'universal-wp'),
    		"dependency" => array(
        		"element" => "wow",
        		"value" => "1"
    		),
		),
	    array(
	        'type' => 'dropdown',
	        'heading' => __( 'Animate', 'universal-wp' ),
	        'param_name' => 'wow_animate',
	        'value' => array(
	            __( 'fadeIn', 'universal-wp' ) => 'fadeIn',
	            __( 'slideInUp', 'universal-wp' ) => 'slideInUp',
	            __( 'zoomIn', 'universal-wp' ) => 'zoomIn',
	        ),
			'std' => 'fadeIn',
            "group" => __("Animate", 'universal-wp'),
    		"dependency" => array(
        		"element" => "wow",
        		"value" => "1"
    		),
	    ),
    ),
    "js_view" => 'VcColumnView',
) );






