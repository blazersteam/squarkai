<?php 
/*Fun Facts*/
add_shortcode('universal_fun', 'universal_fun_f');
function universal_fun_f( $atts, $content = null)
{

	extract(shortcode_atts(
		array(
			'universal_fun_text' => 'Themes Released',
			'universal_fun_count' => '29',
			'universal_fun_count_delay' => '5',
			'universal_fun_count_increment' => '1',
			'white' => null,
			"css" => null
		), $atts)
	);

	if ($white) $white = 'white';

	$output ='<div class="stats-universal">
                  <div class="stats-block stats-top">
                    <div class="stats-desc '. esc_attr($white) .'">
                      <div class="stats-number number-counter"><span data-min="0" data-max="'. esc_attr($universal_fun_count) .'" data-delay="'. esc_attr($universal_fun_count_delay) .'" data-increment="'. esc_attr($universal_fun_count_increment) .'" class="numscroller">0</span></div>
                      <h5 class="no-pad">'. esc_attr($universal_fun_text) .'</h5>
                    </div>
                  </div>
                </div>';
	return $output;


};

/*Fun Facts*/
vc_map( array(
	"name" => __("Fun Facts (Countdown)",'universal-wp'),
	"base" => "universal_fun",
	"category" => __('Universal','universal-wp'),
	"params" => array(
		array(
			"type" => "textfield",
			"admin_label" => true,
			"param_name" => "universal_fun_text",
			"heading" => __("Text", 'universal-wp'),
			"value" => 'Themes Released',
		),
		array(
			"type" => "textfield",
			"admin_label" => true,
			"param_name" => "universal_fun_count",
			"heading" => __("Count", 'universal-wp'),
			"value" => '29',
		),	
		array(
			"type" => "textfield",
			"admin_label" => true,
			"param_name" => "universal_fun_count_delay",
			"heading" => __("Delay (speed)", 'universal-wp'),
			"group" => __("Settings", 'universal-wp'),
			"value" => '5',
		),
		array(
			"type" => "textfield",
			"admin_label" => true,
			"param_name" => "universal_fun_count_increment",
			"heading" => __("Increment", 'universal-wp'),
			"value" => '1',
		),
		array(
    		"type" => "checkbox",
    		"admin_label" => true,
    		"heading" => __("White fonts", 'universal-wp'),
    		"param_name" => "white",
			"value" => array("Yes" => true),
 		),	
	)
) );