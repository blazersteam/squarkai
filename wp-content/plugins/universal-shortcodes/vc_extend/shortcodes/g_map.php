<?php
/*GOOGLE MAP*/
add_shortcode('vc_g_map', 'vc_g_map_f');
function vc_g_map_f( $atts, $content = null)
{
  extract(shortcode_atts(
    array(
      'api_key' => null,
      'dark' => null,
      'address' => '235 Bowery, New York, NY',
      'zoom' => '16',
      'height' => '430px',
    ), $atts)
  );

if($api_key == true) {
  $output ='<div id="map" style="height:'.$height.'"></div>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key='.$api_key.'"></script>
         <script>
var geocoder;
var map;
var marker;
var address = "'.$address.'";

function initialize() {
  geocoder = new google.maps.Geocoder();
  var latlng = new google.maps.LatLng(-34.397, 150.644);

  var myOptions = {
    zoom: '.$zoom.',
    center: latlng,
    disableDefaultUI: true,
    scrollwheel: false,
    draggable: false,';
    if($dark == true){
      $output .='styles:[{"stylers":[{"hue":"#ff1a00"},{"invert_lightness":true},{"saturation":-100},{"lightness":33},{"gamma":0.5}]},{"featureType":"water","elementType":"geometry","stylers":[{"color":"#2D333C"}]}],';
    } else {
      $output .='styles:[{"stylers":[{"saturation":-100},{"gamma":1}]},{"elementType":"labels.text.stroke","stylers":[{"visibility":"off"}]},{"featureType":"poi.business","elementType":"labels.text","stylers":[{"visibility":"off"}]},{"featureType":"poi.business","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"poi.place_of_worship","elementType":"labels.text","stylers":[{"visibility":"off"}]},{"featureType":"poi.place_of_worship","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"geometry","stylers":[{"visibility":"simplified"}]},{"featureType":"water","stylers":[{"visibility":"on"},{"saturation":-35},{"gamma":8},{"hue":"#f7f7f7"}]},{"featureType":"administrative.neighborhood","elementType":"labels.text.fill","stylers":[{"color":"#333333"}]},{"featureType":"road.local","elementType":"labels.text","stylers":[{"weight":0.5},{"color":"#333333"}]},{"featureType":"transit.station","elementType":"labels.icon","stylers":[{"hue": "#0008ff"},{"lightness": "8"},{"saturation": "-20"}]}],';
    };
$output .='mapTypeControl: false,
    mapTypeControlOptions: {
      style: google.maps.MapTypeControlStyle.DROPDOWN_MENU
    },
    navigationControl: false,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  };

  map = new google.maps.Map(document.getElementById("map"), myOptions);

  if (geocoder) {
    geocoder.geocode({
      "address": address
    }, function(results, status) {
      if (status == google.maps.GeocoderStatus.OK) {
        if (status != google.maps.GeocoderStatus.ZERO_RESULTS) {
          map.setCenter(results[0].geometry.location);

          var infowindow = new google.maps.InfoWindow({
            content: "<b>" + address + "</b>",
            size: new google.maps.Size(150, 50)
          });

          var marker = new google.maps.Marker({
            position: results[0].geometry.location,
            map: map,';
    if($dark == true){
      $output .='icon: "'.get_template_directory_uri().'/assets/images/map-marker-light.png",';
    } else {
      $output .='icon: "'.get_template_directory_uri().'/assets/images/map-marker.png",';
    };
$output .='
            title: address
          });
          google.maps.event.addListener(marker, "click", function() {
            infowindow.open(map, marker);
          });
        } else {
          alert("No results found");
        }
      } else {
        alert("Geocode was not successful for the following reason: " + status);
      }
    });
  }
};
google.maps.event.addDomListener(window, "load", initialize);</script>';
} else {
    $output ='<div id="map" style="height:'.$height.'"><p style="text-align: center;margin-bottom: 0;line-height: '.$height.';font-weight: 600;font-size: 21px;background:#f4f4f4;">Please, set up your API key for display Google Maps.</p></div>';;
};
  return $output;
};


vc_map( array(
  "name" => __("Google Map",'universal-wp'),
  "base" => "vc_g_map",
  "category" => __('Universal','universal-wp'),
  "params" => array(
    array(
      "type" => "textfield",
      "admin_label" => true,
      "param_name" => "api_key",
      "heading" => __("API Key", 'universal-wp'),
      "description" => 'Enter your own key for enable Google Maps. <a href="https://developers.google.com/maps/documentation/javascript/get-api-key" target="_blank">Create new key.</a>', 'universal-wp',
    ),
    array(
      "type" => "textfield",
      "admin_label" => true,
      "param_name" => "address",
      "heading" => __("Addres", 'universal-wp'),
      "value" => '235 Bowery, New York, NY',
    ),
    array(
      "type" => "textfield",
      "admin_label" => true,
      "param_name" => "zoom",
      "heading" => __("Zoom", 'universal-wp'),
      "value" => '16',
    ),
    array(
      "type" => "textfield",
      "admin_label" => true,
      "param_name" => "height",
      "heading" => __("Map Height", 'universal-wp'),
      "value" => '430px',
    ),
        array(
      "type" => "checkbox",
      "admin_label" => true,
      "heading" => __("Dark Version", 'universal-wp'),
      "param_name" => "dark",
      "value" => array("Yes" => true),
    ),  
  )
) );