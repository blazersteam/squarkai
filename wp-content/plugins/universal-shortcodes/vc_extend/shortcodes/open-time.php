<?php 
add_shortcode('universal_open_time', 'universal_open_time_f');
function universal_open_time_f( $atts, $content = null)
{

	extract(shortcode_atts(
		array(
			'universal_open_icon' => 'ion-ios-clock-outline',
			'universal_icon_color' => '#ff4081',
			'universal_open_day' => 'Mon',
			'universal_open_time' => '09:00 - 19:00',
			"css" => null
		), $atts)
	);
	
	$output ='<h4><i style="color: '. esc_attr($universal_icon_color) .'" class="icon-big '. esc_attr($universal_open_icon) .'"></i> '. esc_attr($universal_open_day) .'</h4>
            	<p>'. esc_attr($universal_open_time) .'</p>';
	return $output;


};

vc_map( array(
	"name" => __("Schedule",'universal-wp'),
	"base" => "universal_open_time",
	"category" => __('Universal','universal-wp'),
	"params" => array(
		array(
			"type" => "textfield",
			"admin_label" => true,
			"param_name" => "universal_open_icon",
			"heading" => __("Icon", 'universal-wp'),
			"value" => 'ion-ios-clock-outline',
			'description' => __( 'Select icon from <a href="https://dankov-themes.com/icon/universal/index.html" target="_blank">here</a> or <a href="https://fontawesome.com/v4.7.0/icons/" target="_blank">here</a>', 'universal-wp' ),
		),
		array(
			"type" => "colorpicker",
			"admin_label" => true,
			"param_name" => "universal_icon_color",
			"heading" => __("Icon Color", 'universal-wp'),
			"value" => '#ff4081',
		),
		array(
			"type" => "textfield",
			"admin_label" => true,
			"param_name" => "universal_open_day",
			"heading" => __("Day", 'universal-wp'),
			"value" => 'Mon',
		),
		array(
			"type" => "textfield",
			"admin_label" => true,
			"param_name" => "universal_open_time",
			"heading" => __("Time", 'universal-wp'),
			"value" => '09:00 - 19:00',
		),
	)
) );