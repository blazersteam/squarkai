<?php 
/*Pricing Tables*/
add_shortcode('universal_pricing_tables', 'universal_pricing_tables_f');
function universal_pricing_tables_f( $atts, $content = null)
{

	extract(shortcode_atts(
		array(
			'universal_pricing_tables_name' => 'Free',
			'universal_pricing_tables_price' => '7',
			'universal_pricing_tables_currency' => '$',
			'universal_pricing_tables_date' => 'day',
			'universal_pricing_tables_desc1' => '24/7 Tech Support',
			'universal_pricing_tables_desc2' => '80 GB Storage',
			'universal_pricing_tables_desc3' => '1 GB Bandwidth',
			'universal_pricing_tables_desc4' => '100 GB Storage',
			'universal_pricing_tables_button_a' => 'http://google.com/',
			'universal_pricing_tables_button' => 'Get Started',
			'best' => null,
			"css" => null
		), $atts)
	);
	
	if ($best) $best = 'best';

	$output ='<div class="pricing_tables_wrap '. esc_attr($best) .'">
                	<div class="pricing_tables_name">'. esc_attr($universal_pricing_tables_name) .'</div>
                	<div class="pricing_tables_price"><span>'. esc_attr($universal_pricing_tables_currency) .'</span>'. esc_attr($universal_pricing_tables_price) .'<i>/ '. esc_attr($universal_pricing_tables_date) .'</i></div>
                	<div class="pricing_tables_desc">
                		<ul>
                			<li>'. esc_attr($universal_pricing_tables_desc1) .'</li>
                			<li>'. esc_attr($universal_pricing_tables_desc2) .'</li>
                			<li>'. esc_attr($universal_pricing_tables_desc3) .'</li>
                			<li>'. esc_attr($universal_pricing_tables_desc4) .'</li>
                		</ul>
                	</div>
                	<div class="pricing_tables_buttons"><a href="'. esc_url($universal_pricing_tables_button_a) .'">'. esc_attr($universal_pricing_tables_button) .'</a></div>
              </div>';
	return $output;


};

/*Pricing Tables*/
vc_map( array(
	"name" => __("Pricing Tables",'universal-wp'),
	"base" => "universal_pricing_tables",
	"category" => __('Universal','universal-wp'),
	"params" => array(
		array(
			"type" => "textfield",
			"admin_label" => true,
			"param_name" => "universal_pricing_tables_name",
			"heading" => __("Name", 'universal-wp'),
			"value" => 'Free',
		),
		array(
			"type" => "textfield",
			"admin_label" => true,
			"param_name" => "universal_pricing_tables_price",
			"heading" => __("Price", 'universal-wp'),
			"value" => '7',
		),
		array(
			"type" => "textfield",
			"admin_label" => true,
			"param_name" => "universal_pricing_tables_currency",
			"heading" => __("Сurrency", 'universal-wp'),
			"value" => '$',
		),
		array(
			"type" => "textfield",
			"admin_label" => true,
			"param_name" => "universal_pricing_tables_date",
			"heading" => __("Time", 'universal-wp'),
			"value" => 'Day',
		),
		array(
			"type" => "textfield",
			"admin_label" => true,
			"param_name" => "universal_pricing_tables_desc1",
			"heading" => __("Value 1", 'universal-wp'),
			"value" => '24/7 Tech Support',
		),
		array(
			"type" => "textfield",
			"admin_label" => true,
			"param_name" => "universal_pricing_tables_desc2",
			"heading" => __("Value 2", 'universal-wp'),
			"value" => '80 GB Storage',
		),
		array(
			"type" => "textfield",
			"admin_label" => true,
			"param_name" => "universal_pricing_tables_desc3",
			"heading" => __("Value 3", 'universal-wp'),
			"value" => '1 GB Bandwidth',
		),
		array(
			"type" => "textfield",
			"admin_label" => true,
			"param_name" => "universal_pricing_tables_desc4",
			"heading" => __("Value 4", 'universal-wp'),
			"value" => '100 GB Storage',
		),
		array(
			"type" => "textfield",
			"admin_label" => true,
			"param_name" => "universal_pricing_tables_button_a",
			"heading" => __("Button Link", 'universal-wp'),
			"value" => 'http://google.com/',
		),
		array(
			"type" => "textfield",
			"admin_label" => true,
			"param_name" => "universal_pricing_tables_button",
			"heading" => __("Button", 'universal-wp'),
			"value" => 'Get Started',
		),
        array(
			"type" => "checkbox",
			"admin_label" => true,
			"heading" => __("Best Options", 'universal-wp'),
			"param_name" => "best",
			"value" => array("Yes" => true),
		),	
	)
) );




