<?php 
/*Promo Title*/
add_shortcode('universal_promo', 'universal_promo_f');
function universal_promo_f( $atts, $content = null)
{

	extract(shortcode_atts(
		array(
			'universal_promo_text' => 'Our Services',
			'universal_promo_paragraph' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla convallis pulvinar vestibulum. Doneceleifend, sem sed dictum. Lorem ipsum dolor sit amet, consectetur adipiscing elits',
			'center' => null,
			'white' => null,
			"css" => null
		), $atts)
	);
	
	if ($center) $center = 'center';
	if ($white) $white = 'white';

	$output ='<div class="promo-block '. esc_attr($center) .' '. esc_attr($white) .'">
            	<h3>'. esc_attr($universal_promo_text) .'</h3>
            	<p>'. esc_attr($universal_promo_paragraph) .'</p>
              </div>';
	return $output;


};

/*Promo Title*/
vc_map( array(
	"name" => __("Promo Title",'universal-wp'),
	"base" => "universal_promo",
	"category" => __('Universal','universal-wp'),
	"params" => array(
		array(
			"type" => "textfield",
			"admin_label" => true,
			"param_name" => "universal_promo_text",
			"heading" => __("Title", 'universal-wp'),
			"value" => 'Our Services',
		),
		array(
			"type" => "textarea",
			"admin_label" => true,
			"param_name" => "universal_promo_paragraph",
			"heading" => __("Paragraph", 'universal-wp'),
			"value" => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla convallis pulvinar vestibulum. Doneceleifend, sem sed dictum. Lorem ipsum dolor sit amet, consectetur adipiscing elits',
		),
        array(
			"type" => "checkbox",
			"admin_label" => true,
			"heading" => __("White fonts", 'universal-wp'),
			"param_name" => "white",
			"value" => array("Yes" => true),
		),	
        array(
			"type" => "checkbox",
			"admin_label" => true,
			"heading" => __("Text Center", 'universal-wp'),
			"param_name" => "center",
			"value" => array("Yes" => true),
		),	
	)
) );