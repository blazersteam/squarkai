<?php 
/*Quote*/
add_shortcode('universal_quote', 'universal_quote_f');
function universal_quote_f( $atts, $content = null)
{

	extract(shortcode_atts(
		array(
			'universal_quote_text' => 'Teach self-denial and make its practice pleasure, and you can create for the world a destiny more sublime that ever issued from the brain of the wildest dreamer.',
			'universal_quote_author' => 'Sir Walter Scott',
			'center' => null,
			'white' => null,
			"css" => null
		), $atts)
	);
	
	if ($center) $center = 'center';
	if ($white) $white = 'white';

	$output ='<div class="quote-block '. esc_attr($center) .' '. esc_attr($white) .'">
	            <p><i class="icon fa fa-quote-left fa-lg"></i></p>
            	<p>'. esc_attr($universal_quote_text) .'</p>
            	<h2 class="no-pad classic">'. esc_attr($universal_quote_author) .'</h2>
              </div>';
	return $output;


};


/*Quote*/
vc_map( array(
	"name" => __("Quote",'universal-wp'),
	"base" => "universal_quote",
	"category" => __('Universal','universal-wp'),
	"params" => array(
		array(
			"type" => "textarea",
			"admin_label" => true,
			"param_name" => "universal_quote_text",
			"heading" => __("Quote", 'universal-wp'),
			"value" => 'Teach self-denial and make its practice pleasure, and you can create for the world a destiny more sublime that ever issued from the brain of the wildest dreamer.',
		),
		array(
			"type" => "textfield",
			"admin_label" => true,
			"param_name" => "universal_quote_author",
			"heading" => __("Author", 'universal-wp'),
			"value" => 'Sir Walter Scott',
		),
        array(
			"type" => "checkbox",
			"admin_label" => true,
			"heading" => __("White fonts", 'universal-wp'),
			"param_name" => "white",
			"value" => array("Yes" => true),
		),	
        array(
			"type" => "checkbox",
			"admin_label" => true,
			"heading" => __("Text Center", 'universal-wp'),
			"param_name" => "center",
			"value" => array("Yes" => true),
		),	
	)
) );




