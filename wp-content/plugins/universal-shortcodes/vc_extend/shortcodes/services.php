<?php 
/*Services*/
add_shortcode('universal_services', 'universal_services_f');
function universal_services_f( $atts, $content = null)
{

	extract(shortcode_atts(
		array(
			'type_cont' => 'Icon',
			'universal_icons' => 'ion-ios-analytics-outline',
			'universal_image' => null,
			'universal_name' => 'Analytics',
			'universal_text' => 'Lorem ipsum dolor sit amet. Con eleifend sem sed dictum mattis sectetur elit. Nulla convallis pul.',
			'link' => '#',
			'white' => null,
			'wow' => null,
			'wow_delay' => '0.1',
			'wow_animate' => 'fadeIn',
			"css" => null
		), $atts)
	);

	if ($wow) $wow = 'wow';
	if ($white) $white = 'white';

    $image = wp_get_attachment_image_src($universal_image, true);
    $image = $image[0];


	$output ='<div class="'. esc_attr($wow) .' '. esc_attr($wow_animate) .'" data-wow-delay="'. esc_attr($wow_delay) .'s">
				<div class="hi-icon-effect '. esc_attr($white) .'">';
					if($type_cont =="Icon"){
						$output .='<div class="hi-icon"><a href="'. esc_url($link) .'"><i class="'. esc_attr($universal_icons) .'"></i></a></div>';
					} else {
						$output .='<div class="hi-icon image"><a href="'. esc_url($link) .'"><img src="'. esc_url($image) .'" alt=""></a></div>';
					};
	$output .='<div class="service-name">'. esc_attr($universal_name) .'</div>
	              	<div class="service-text">'. esc_attr($universal_text) .'</div>
	            </div>
            </div>';

	return $output;
};

vc_map( array(
	"name" => __("Services Style #1",'universal-wp'),
	"base" => "universal_services",    
	"category" => __('Pheromone','universal-wp'),
	"params" => array(
		array(
			"type" => "dropdown",
			"admin_label" => true,
			"heading" => __("Type", 'universal-wp'),
			"param_name" => "type_cont",
	        'value' => array(
	            __( 'Icon', 'universal-wp' ) => 'icon',
	            __( 'Image', 'universal-wp' ) => 'image',
	        ),
		),
		array(
			"type" => "textfield",
			"admin_label" => true,
			"param_name" => "universal_icons",
			"heading" => __("Icon", 'universal-wp'),
			"value" => 'ion-ios-analytics-outline',
			'description' => __( 'Select icon from <a href="https://dankov-themes.com/icon/universal/" target="_blank">here</a> or <a href="https://fontawesome.com/v4.7.0/icons/" target="_blank">here</a>', 'universal-wp' ),
    		"dependency" => array(
        		"element" => "type_cont",
        		"value" => "icon"
    		),
		),
	    array(
			"type" => "attach_image",
			"admin_label" => true,
			"param_name" => "universal_image",
			"heading" => __("Image", 'universal-wp'),
			"value" => '',
    		"dependency" => array(
        		"element" => "type_cont",
        		"value" => "image"
    		),
	    ),
		array(
			"type" => "textfield",
			"admin_label" => true,
			"param_name" => "link",
			"heading" => __("Link", 'universal-wp'),
			"value" => '#',
		),
		array(
			"type" => "textfield",
			"admin_label" => true,
			"param_name" => "universal_name",
			"heading" => __("Title", 'universal-wp'),
			"value" => 'Analytics',
		),
		array(
			"type" => "textfield",
			"admin_label" => true,
			"param_name" => "universal_text",
			"heading" => __("Description", 'universal-wp'),
			"value" => 'Lorem ipsum dolor sit amet. Con eleifend sem sed dictum mattis sectetur elit. Nulla convallis pul.',
		),
		array(
			"type" => "checkbox",
			"admin_label" => true,
			"heading" => __("White color", 'universal-wp'),
			"param_name" => "white",
			"value" => array("Yes" => true),
            "group" => __("Settings", 'universal-wp'),
		),
		array(
			"type" => "checkbox",
			"heading" => __("Animate", 'universal-wp'),
			"param_name" => "wow",
			"value" => array("Yes" => true),
            "group" => __("Settings", 'universal-wp'),
		),
		array(
			"type" => "textfield",
			"heading" => __("Delay", 'universal-wp'),
			"param_name" => "wow_delay",
			"value" => '100',
			"description" => 'in s',
            "group" => __("Settings", 'universal-wp'),
    		"dependency" => array(
        		"element" => "wow",
        		"value" => "1"
    		),
		),
	    array(
	        'type' => 'dropdown',
	        'heading' => __( 'Animate', 'universal-wp' ),
	        'param_name' => 'wow_animate',
	        'value' => array(
	            __( 'fadeIn', 'universal-wp' ) => 'fadeIn',
	            __( 'slideInUp', 'universal-wp' ) => 'slideInUp',
	        ),
			'std' => 'fadeIn',
            "group" => __("Settings", 'universal-wp'),
    		"dependency" => array(
        		"element" => "wow",
        		"value" => "1"
    		),
	    ),
	)
) 
);