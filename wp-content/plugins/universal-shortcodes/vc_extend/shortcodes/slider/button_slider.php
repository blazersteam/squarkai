<?php
add_shortcode('vc_button_slider', 'vc_button_slider_f');
function vc_button_slider_f( $atts, $content = null)
{
	extract(shortcode_atts(
		array(
			'text' => 'Who We Are',
			'href' => '#about',
			'margin' => '40px',
			'white' => null,
		), $atts)
	);

	if ($white) $white = 'btn-white';

    $output ='<a href="'.$href.'" style="margin-top: '.$margin.';margin-left: 10px;margin-right:10px;" class="btn '. esc_attr($white) .' btn-border btn-lg page-scroll">'.$text.'</a>';

	return $output;
};


vc_map( array(
	"name" => __("Button Item",'universal-wp'),
	"base" => "vc_button_slider",
    "content_element" => true,
    "as_child" => array('only' => 'universal_hero_image, universal_hero_video, universal_hero_kenburns'), 
	"category" => __('Headers','universal-wp'),
	"params" => array(
		array(
			"type" => "textfield",
			"admin_label" => true,
			"param_name" => "text",
			"heading" => __("Button Name", 'universal-wp'),
			"value" => 'Who We Are',
		),
		array(
			"type" => "textfield",
			"admin_label" => true,
			"param_name" => "href",
			"heading" => __("Button Href", 'universal-wp'),
			"description" => __("You need to add the same value in first block in VC", 'universal-wp'),
			"value" => '#about',
		),	
		array(
			"type" => "textfield",
			"admin_label" => true,
			"param_name" => "margin",
			"heading" => __("Margin Top", 'universal-wp'),
			"value" => '40px',
		),	
        array(
			"type" => "checkbox",
			"admin_label" => true,
			"heading" => __("White background", 'universal-wp'),
			"param_name" => "white",
			"value" => array("Yes" => true),
		),	
	)
) );