<?php
add_shortcode('vc_image_slider', 'vc_image_slider_f');
function vc_image_slider_f( $atts, $content = null)
{
	extract(shortcode_atts(
		array(
			'type_cont' => 'Image',
			'slider_image' => null,
			'slider_icon' => 'fa fa-barcode',
			'slider_icon_size' => '80px',
			'slider_icon_line_height' => '120px',
		), $atts)
	);

    $image = wp_get_attachment_image_src($slider_image, true);
    $image = $image[0];

    if($type_cont =="Image"){
		$output ='<p><img src="'. esc_url($image) .'" alt=""></p>';
	} else {
		$output ='<i class="'. esc_attr($slider_icon) .'" style="font-size: '. esc_attr($slider_icon_size) .'; line-height: '. esc_attr($slider_icon_line_height) .';"></i>';
	};
	return $output;
};

vc_map( array(
	"name" => __("Image/Icon Item",'universal-wp'),
	"base" => "vc_image_slider",
    "content_element" => true,
    "as_child" => array('only' => 'universal_hero_image, universal_hero_video, universal_hero_kenburns'), // Use only|except attributes to limit parent (separate multiple values with comma)
	"category" => __('Headers','universal-wp'),
	"params" => array(
		array(
			"type" => "dropdown",
			"admin_label" => true,
			"heading" => __("Type", 'universal-wp'),
			"param_name" => "type_cont",
	        'value' => array(
	            __( 'Image', 'universal-wp' ) => 'image',
	            __( 'Icon', 'universal-wp' ) => 'icon',
	        ),
		),
		array(
			"type" => "attach_image",
			"admin_label" => true,
			"param_name" => "slider_image",
			"heading" => __("Image", 'universal-wp'),
    		"dependency" => array(
        		"element" => "type_cont",
        		"value" => "image"
    		),
		),
		array(
			"type" => "textfield",
			"admin_label" => true,
			"param_name" => "slider_icon",
			"heading" => __("Icon", 'universal-wp'),
			"value" => 'fa fa-barcode',
			'description' => __( 'Select icon from <a href="https://dankov-themes.com/icon/universal/index.html" target="_blank">here</a> or <a href="https://fontawesome.com/v4.7.0/icons/" target="_blank">here</a>', 'universal-wp' ),
    		"dependency" => array(
        		"element" => "type_cont",
        		"value" => "icon"
    		),
		),
		array(
			"type" => "textfield",
			"admin_label" => true,
			"param_name" => "slider_icon_size",
			"heading" => __("Icon Size in px", 'universal-wp'),
			"value" => '80px',
    		"dependency" => array(
        		"element" => "type_cont",
        		"value" => "icon"
    		),
		),
		array(
			"type" => "textfield",
			"admin_label" => true,
			"param_name" => "slider_icon_line_height",
			"heading" => __("Line Height in px", 'universal-wp'),
			"value" => '120px',
    		"dependency" => array(
        		"element" => "type_cont",
        		"value" => "icon"
    		),
		),	
	)
) );