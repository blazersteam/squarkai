<?php 
/*MailChimp*/
add_shortcode('vc_mailchimp_slider', 'vc_mailchimp_slider_f');
function vc_mailchimp_slider_f( $atts, $content = null)
{

	extract(shortcode_atts(
		array(
			'universal_slider_mailchimp_action' => 'https://forbetterweb.us11.list-manage.com/subscribe/post?u=4f751a6c58b225179404715f0&amp;id=18fc72763a',
			'universal_slider_mailchimp_text' => 'GET NOTIFIED!',
			'universal_slider_mailchimp_placeholder' => 'Email address...',
			'universal_slider_mailchimp_button' => 'btn-violet',
			"css" => null
		), $atts)
	);
	
	$output ='<form id="mc-embedded-subscribe-form2" action="'. esc_url($universal_slider_mailchimp_action) .'" method="post" name="mc-embedded-subscribe-form" target="_blank" novalidate="" class="small-form subscribe-form">
                <div class="input-group input-group-lg">
                  <input id="mce-EMAIL2" type="email" name="EMAIL" placeholder="'. esc_attr($universal_slider_mailchimp_placeholder) .'" class="form-control"><span class="input-group-btn">
                    <button id="mc-embedded-subscribe2" type="submit" name="subscribe" class="btn '. esc_attr($universal_slider_mailchimp_button) .'">'. esc_attr($universal_slider_mailchimp_text) .'</button></span>
                </div>
                <div id="mce-responses2">
                  <div id="mce-error-response2" style="display:none" class="response"></div>
                  <div id="mce-success-response2" style="display:none" class="response"></div>
                </div>
              </form>';
	return $output;


};

/*MailChimp*/
vc_map( array(
	"name" => __("MailChimp",'universal-wp'),
	"base" => "vc_mailchimp_slider",
    "content_element" => true,
    "as_child" => array('only' => 'universal_hero_image, universal_hero_video, universal_hero_kenburns'), // Use only|except attributes to limit parent (separate multiple values with comma)
	"category" => __('Universal','universal-wp'),
	"params" => array(
		array(
			"type" => "textfield",
			"admin_label" => true,
			"param_name" => "universal_slider_mailchimp_action",
			"heading" => __("Action", 'universal-wp'),
			"value" => 'https://forbetterweb.us11.list-manage.com/subscribe/post?u=4f751a6c58b225179404715f0&amp;id=18fc72763a',
		),	
		array(
			"type" => "textfield",
			"admin_label" => true,
			"param_name" => "universal_slider_mailchimp_text",
			"heading" => __("Text Button", 'universal-wp'),
			"value" => 'GET NOTIFIED!',
		),	
		array(
			"type" => "textfield",
			"admin_label" => true,
			"param_name" => "universal_slider_mailchimp_placeholder",
			"heading" => __("Placeholder", 'universal-wp'),
			"value" => 'Email address...',
		),	
		array(
			"type" => "dropdown",
			"admin_label" => true,
			"heading" => __("Button Color", 'universal-wp'),
			"param_name" => "universal_slider_mailchimp_button",
	        'value' => array(
	            __( 'Pink', 'universal-wp' ) => 'btn-violet',
	            __( 'Gray', 'universal-wp' ) => 'btn-universal',
	            __( 'Black', 'universal-wp' ) => 'btn-dark',
	        ),
	        'std' => 'btn-violet',
		),		
	)
) );