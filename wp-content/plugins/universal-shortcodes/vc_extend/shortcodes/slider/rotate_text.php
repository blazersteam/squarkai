<?php
/*TESTIMONIAL  ITEM*/
add_shortcode('vc_rotate_title', 'vc_rotate_title_f');
function vc_rotate_title_f( $atts, $content = null)
{
	extract(shortcode_atts(
		array(
			'title' => 'Smart, Minimal, Easy to use, Fast loading, Lightweight, Multipurpose',
		), $atts)
	);

    $output ='<h1><span class="rotate">'.$title.'</span></h1>';

	return $output;
};


vc_map( array(
	"name" => __("Rotate Text Item",'universal-wp'),
	"base" => "vc_rotate_title",
    "content_element" => true,
    "as_child" => array('only' => 'universal_hero_image, universal_hero_video, universal_hero_kenburns'), // Use only|except attributes to limit parent (separate multiple values with comma)
	"category" => __('Headers','universal-wp'),
	"params" => array(
		array(
			"type" => "textfield",
			"admin_label" => true,
			"param_name" => "title",
			"heading" => __("Title", 'universal-wp'),
			"value" => 'Smart, Minimal, Easy to use, Fast loading, Lightweight, Multipurpose',
		),		
	)
) );