<?php
add_shortcode('vc_slide_slider', 'vc_slide_slider_f');
function vc_slide_slider_f( $atts, $content = null)
{
	extract(shortcode_atts(
		array(
			'universal_image' => null,
			'universal_type' => 'video',
			'universal_video_link' => 'https://vimeo.com/153485166',
			'universal_video_buttons_one' => 'Who We Are',
			'universal_video_buttons_two' => 'Contacts Us',
			'universal_video_buttons_one_href' => '#about',
			'universal_video_buttons_two_href' => '#contact',
			'universal_mouse' => true,
			'universal_video_mouse_href' => '#about',
			'universal_sub_title' => 'Responsive Business Theme',
			'universal_title' => 'Business Startup',
		), $atts)
	);




    $image = wp_get_attachment_image_src($universal_image, true);
    $image = $image[0];

    $output ='<div class="item">
          		<div style="background-image:url('. esc_url($image) .');" class="fill">
		            <div class="intro-body">';
						if($universal_type == 'video'){
					        $output .='<a href="'. esc_url($universal_video_link) .'" data-rel="video" class="swipebox-video">
					            			<i class="icon-big ion-ios-play-outline wow fadeInUp"></i>
					            		</a>
					                	<h1 class="wow fadeInDown">'.esc_attr($universal_title).'</h1>
					                	<h4 class="wow fadeInUp">'.esc_attr($universal_sub_title).'</h4>';
						} elseif ($universal_type == 'buttons')  {
							$output .='<h4 class="wow fadeInUp">'.esc_attr($universal_sub_title).'</h4>
					              		<h1 class="wow fadeInDown">'.esc_attr($universal_title).'</h1>
							              <ul class="list-inline lead">
							                <li><a href="'.esc_attr($universal_video_buttons_one_href).'" class="btn btn-border btn-lg page-scroll wow fadeInLeft"><i class="fa fa-chevron-down"></i> '.esc_attr($universal_video_buttons_one).'</a></li>
							                <li><a href="'.esc_attr($universal_video_buttons_two_href).'" class="btn btn-white btn-lg page-scroll wow fadeInRight"><i class="fa fa-chevron-down"></i> '.esc_attr($universal_video_buttons_two).'</a></li>
							              </ul>';
						} elseif ($universal_type == 'standard')  {
					        $output .='<h4 data-wow-delay=".4s" class="wow fadeInUp">'.esc_attr($universal_sub_title).'</h4>
					              <h1 class="wow fadeInDown">'.esc_attr($universal_title).'</h1>';
					              if($universal_mouse == true){$output .='<div data-wow-delay="1s" class="scroll-btn hidden-xs wow fadeInDown"><a href="'.esc_attr($universal_video_mouse_href).'" class="page-scroll"><span class="mouse"><span class="weel"><span></span></span></span></a></div>';};
					    };
		$output .='</div>
          		</div>
        	</div>';

	return $output;
};

        
vc_map( array(
	"name" => __("Slider Item",'universal-wp'),
	"base" => "vc_slide_slider",
    "content_element" => true,
    "as_child" => array('only' => 'universal_hero_slider'), // Use only|except attributes to limit parent (separate multiple values with comma)
	"category" => __('Headers','universal-wp'),
	"params" => array(
        array(
            "type" => "dropdown",
            "admin_label" => true,
            "heading" => __("Slide Type", 'universal-wp'),
            "param_name" => "universal_type",
            'value' => array(
                __( 'Video Type', 'universal-wp' ) => 'video',
                __( 'Button Type', 'universal-wp' ) => 'buttons',
                __( 'Standard Type', 'universal-wp' ) => 'standard',
            ),
        ),
	    array(
			"type" => "attach_image",
			"param_name" => "universal_image",
			"heading" => __("Image", 'universal-wp'),
	    ),	
        array(
            "type" => "textfield",
    		"heading" => __("Title", 'universal-wp'),
            "param_name" => "universal_title",
            "value" => 'Business Startup', 
        ),
        array(
            "type" => "textfield",
    		"heading" => __("Sub Title", 'universal-wp'),
            "param_name" => "universal_sub_title",
            "value" => 'Responsive Business Theme', 
        ),
        array(
            "type" => "textfield",
    		"heading" => __("Video Url", 'universal-wp'),
            "param_name" => "universal_video_link",
            "value" => 'https://vimeo.com/153485166', 
    		"dependency" => array(
        		"element" => "universal_type",
        		"value" => 'video',
    		),
        ),
        array(
            "type" => "textfield",
            "param_name" => "universal_video_buttons_one",
            "value" => 'Who We Are', 
    		"dependency" => array(
        		"element" => "universal_type",
        		"value" => 'buttons',
    		),
        ),
        array(
            "type" => "textfield",
    		"heading" => __("Button Href", 'universal-wp'),
            "param_name" => "universal_video_buttons_one_href",
            "value" => '#about', 
    		"dependency" => array(
        		"element" => "universal_type",
        		"value" => 'buttons',
    		),
        ),
        array(
            "type" => "textfield",
            "param_name" => "universal_video_buttons_two",
            "value" => 'Contacts Us', 
    		"dependency" => array(
        		"element" => "universal_type",
        		"value" => 'buttons',
    		),
        ),
        array(
            "type" => "textfield",
    		"heading" => __("Button Href", 'universal-wp'),
            "param_name" => "universal_video_buttons_two_href",
            "value" => '#contact', 
    		"dependency" => array(
        		"element" => "universal_type",
        		"value" => 'buttons',
    		),
        ),
		array(
    		"type" => "checkbox",
    		"heading" => __("Animate Mouse", 'universal-wp'),
    		"param_name" => "universal_mouse",
			"value" => array("Yes" => true),
			"std" => true,
    		"dependency" => array(
        		"element" => "universal_type",
        		"value" => 'standard',
    		),
 		),
        array(
            "type" => "textfield",
    		"heading" => __("Animate Mouse Href", 'universal-wp'),
            "param_name" => "universal_video_mouse_href",
            "value" => '#about', 
    		"dependency" => array(
        		"element" => "universal_type",
        		"value" => 'standard',
    		),
        ),		
	)
) );