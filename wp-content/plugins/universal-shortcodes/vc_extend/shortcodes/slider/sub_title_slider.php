<?php

add_shortcode('vc_sub_title_slider', 'vc_sub_title_slider_f');
function vc_sub_title_slider_f( $atts, $content = null)
{
	extract(shortcode_atts(
		array(
			'font' => 'h4',
			'sub_title' => 'Smart Business Template',
			'title_font_family' => null,
			'classic' => null,
		), $atts)
	);

	if ($classic) $classic = 'classic';

	if($font =="h2"){
			$output ='<h2 style="font-family: '. esc_attr($title_font_family) .';" class="'. esc_attr($classic) .'">'.esc_attr($sub_title).'</h2>';
		} elseif ($font =="h3") {
			$output ='<h3 style="font-family: '. esc_attr($title_font_family) .';" class="'. esc_attr($classic) .'">'.esc_attr($sub_title).'</h3>';
		} elseif ($font =="h4") {
			$output ='<h4 style="font-family: '. esc_attr($title_font_family) .';" class="'. esc_attr($classic) .'">'.esc_attr($sub_title).'</h4>';
	};
	
	return $output;
};

vc_map( array(
	"name" => __("Sub Title Item",'universal-wp'),
	"base" => "vc_sub_title_slider",
    "content_element" => true,
    "as_child" => array('only' => 'universal_hero_image, universal_hero_video, universal_hero_kenburns'), // Use only|except attributes to limit parent (separate multiple values with comma)
	"category" => __('Headers','universal-wp'),
	"params" => array(
	    array(
	        'type' => 'dropdown',
	        'heading' => __( 'Heading', 'universal-wp' ),
	        'param_name' => 'font',
	        'value' => array(
	            __( 'H2', 'universal-wp' ) => 'h2',
	            __( 'H3', 'universal-wp' ) => 'h3',
	            __( 'H4', 'universal-wp' ) => 'h4',
	        ),
			'std' => 'h4',
	    ),
		array(
			"type" => "textfield",
			"admin_label" => true,
			"param_name" => "sub_title",
			"heading" => __("Sub Title", 'universal-wp'),
			"value" => 'Smart Business Template',
		),	
		array(
			"type" => "dropdown",
			"admin_label" => true,
			"param_name" => "title_font_family",
			"heading" => __("Font Family", 'universal-wp'),
	        'value' => array(
	            __( 'Caveat', 'universal-wp' ) => 'Caveat',
	            __( 'Great-Vibes', 'universal-wp' ) => 'Great Vibes',
	            __( 'Raleway', 'universal-wp' ) => 'Raleway',
	        ),
			'std' => 'Raleway',
		),
		array(
			"type" => "checkbox",
			"admin_label" => true,
			"heading" => __("Rotate Font", 'universal-wp'),
			"param_name" => "classic",
			"value" => array("Yes" => true),
		),
	)
) );