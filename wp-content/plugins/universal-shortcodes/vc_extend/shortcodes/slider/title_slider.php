<?php
add_shortcode('vc_title_slider', 'vc_title_slider_f');
function vc_title_slider_f( $atts, $content = null)
{
	extract(shortcode_atts(
		array(
			'title' => 'Universal',
			'font' => 'h1',
			'title_size' => null,
			'title_line_height' => null,
			'title_font_family' => 'Raleway',
			'classic' => null,
		), $atts)
	);

	if ($classic) $classic = 'classic';

	if($font =="h1"){
			$output ='<h1 style="font-size: '. esc_attr($title_size) .'; font-family: '. esc_attr($title_font_family) .'; line-height: '. esc_attr($title_line_height) .';" class="'. esc_attr($classic) .'">'.esc_attr($title).'</h1>';
		} elseif ($font =="h2") {
			$output ='<h2 style="font-size: '. esc_attr($title_size) .'; font-family: '. esc_attr($title_font_family) .'; line-height: '. esc_attr($title_line_height) .';" class="'. esc_attr($classic) .'">'.esc_attr($title).'</h2>';
		} elseif ($font =="h3") {
			$output ='<h3 style="font-size: '. esc_attr($title_size) .'; font-family: '. esc_attr($title_font_family) .'; line-height: '. esc_attr($title_line_height) .';" class="'. esc_attr($classic) .'">'.esc_attr($title).'</h3>';
		} elseif ($font =="h4") {
			$output ='<h4 style="font-size: '. esc_attr($title_size) .'; font-family: '. esc_attr($title_font_family) .'; line-height: '. esc_attr($title_line_height) .';" class="'. esc_attr($classic) .'">'.esc_attr($title).'</h4>';
		} elseif ($font =="h5") {
			$output ='<h5 style="font-size: '. esc_attr($title_size) .'; font-family: '. esc_attr($title_font_family) .'; line-height: '. esc_attr($title_line_height) .';" class="'. esc_attr($classic) .'">'.esc_attr($title).'</h5>';
		} elseif ($font =="h6") {
			$output ='<h6 style="font-size: '. esc_attr($title_size) .'; font-family: '. esc_attr($title_font_family) .'; line-height: '. esc_attr($title_line_height) .';" class="'. esc_attr($classic) .'">'.esc_attr($title).'</h6>';
		};

	return $output;
};

vc_map( array(
	"name" => __("Title Item",'universal-wp'),
	"base" => "vc_title_slider",
    "content_element" => true,
    "as_child" => array('only' => 'universal_hero_image, universal_hero_video, universal_hero_kenburns'), // Use only|except attributes to limit parent (separate multiple values with comma)
	"category" => __('Headers','universal-wp'),
	"params" => array(
		array(
			"type" => "textfield",
			"admin_label" => true,
			"param_name" => "title",
			"heading" => __("Title", 'universal-wp'),
			"value" => 'Universal',
		),	
	    array(
	        'type' => 'dropdown',
	        'heading' => __( 'Heading', 'universal-wp' ),
	        'param_name' => 'font',
	        'value' => array(
	            __( 'H1', 'universal-wp' ) => 'h1',
	            __( 'H2', 'universal-wp' ) => 'h2',
	            __( 'H3', 'universal-wp' ) => 'h3',
	            __( 'H4', 'universal-wp' ) => 'h4',
	            __( 'H5', 'universal-wp' ) => 'h5',
	            __( 'H6', 'universal-wp' ) => 'h6',
	        ),
			'std' => 'h1',
	    ),
		array(
			"type" => "textfield",
			"admin_label" => true,
			"param_name" => "title_size",
			"heading" => __("Font Size", 'universal-wp'),
			"value" => '',
		),	
		array(
			"type" => "textfield",
			"admin_label" => true,
			"param_name" => "title_line_height",
			"heading" => __("Line Height", 'universal-wp'),
			"value" => '',
		),	
		array(
			"type" => "dropdown",
			"admin_label" => true,
			"param_name" => "title_font_family",
			"heading" => __("Font Family", 'universal-wp'),
	        'value' => array(
	            __( 'Caveat', 'universal-wp' ) => 'Caveat',
	            __( 'Great-Vibes', 'universal-wp' ) => 'Great Vibes',
	            __( 'Raleway', 'universal-wp' ) => 'Raleway',
	        ),
			'std' => 'raleway',
		),
		array(
			"type" => "checkbox",
			"admin_label" => true,
			"heading" => __("Rotate Font", 'universal-wp'),
			"param_name" => "classic",
			"value" => array("Yes" => true),
		),
		
	)
) );

