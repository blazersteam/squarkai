<?php 
add_shortcode('universal_hero_image', 'universal_hero_image_f');
function universal_hero_image_f( $atts, $content = null)
{
	extract(shortcode_atts(
		array(
			'type_cont' => 'image',
			'universal_size' => 'full',
			'universal_image' => null,
			'universal_color' => '#00bcd4',
			'universal_size_custom' => '500px',
			"css" => null
		), $atts)
	);

    $image = wp_get_attachment_image_src($universal_image, true);
    $image = $image[0];


	if($universal_size == 'full'){
		if($type_cont == 'image'){
			$output ='<div data-background="'. esc_url($image) .'" class="intro full">';
		} else {
			$output ='<div style="background-color:'.$universal_color.';" class="intro full solid-color">';
		};
	} else {
		if($type_cont == 'image'){
			$output ='<div data-background="'. esc_url($image) .'" class="intro" style="height:'.$universal_size_custom.' !important;">';
		} else {
			$output ='<div style="background-color:'.$universal_color.'; height:'.$universal_size_custom.' !important;" class="intro solid-color">';
		};
	};
		$output .='<div class="intro-body">';
		$output .='<div class="container">';
          $output .='<div class="row">';
            $output .='<div class="col-md-6 col-md-offset-3">';
            $output .=''.do_shortcode($content).'';
            $output .='</div>';
          $output .='</div>';
        $output .='</div>';
        $output .='</div>';
        $output .='</div>';



	$output .='<script>
jQuery.noConflict()(function($){
"use strict";

        var introHeader = $(".intro"),
            intro = $(".intro");

        buildModuleHeader(introHeader);

        $(window).resize(function() {
            var width = Math.max($(window).width(), window.innerWidth);
            buildModuleHeader(introHeader);
        });

        $(window).scroll(function() {
            effectsModuleHeader(introHeader, this);
        });

        intro.each(function(i) {
            if ($(this).attr("data-background")) {
                $(this).css("background-image", "url(" + $(this).attr("data-background") + ")");
            }
        });


        function buildModuleHeader(introHeader) {
        };
        function effectsModuleHeader(introHeader, scrollTopp) {
            if (introHeader.length > 0) {
                var homeSHeight = introHeader.height();
                var topScroll = $(document).scrollTop();
                if ((introHeader.hasClass("intro")) && ($(scrollTopp).scrollTop() <= homeSHeight)) {
                    introHeader.css("top", (topScroll * .4));
                }
                if (introHeader.hasClass("intro") && ($(scrollTopp).scrollTop() <= homeSHeight)) {
                    introHeader.css("opacity", (1 - topScroll/introHeader.height() * 1));
                }
            }
        };
});
	</script>';

	return $output;
};

vc_map( array(
	"name" => __("Hero Image", 'universal-wp'),
	"base" => "universal_hero_image",
	"category" => __('Headers', 'universal-wp'),
    "as_parent" => array('only' => 'vc_title_slider, vc_sub_title_slider, vc_mouse_slider, vc_image_slider, vc_rotate_title, vc_button_slider, vc_text_slider, vc_mailchimp_slider, vc_comingsoom_slider'),
    "content_element" => true,
    "show_settings_on_create" => true,
	"params" => array(
		array(
			"type" => "dropdown",
            "admin_label" => true,
			"heading" => __("Background type", 'universal-wp'),
			"param_name" => "type_cont",
	        'value' => array(
	            __( 'Image', 'universal-wp' ) => 'image',
	            __( 'Color', 'universal-wp' ) => 'color',
	        ),
		),
	    array(
			"type" => "attach_image",
            "admin_label" => true,
			"param_name" => "universal_image",
			"heading" => __("Image", 'universal-wp'),
    		"dependency" => array(
        		"element" => "type_cont",
        		"value" => "image"
    		),
	    ),	
		array(
			"type" => "colorpicker",
            "admin_label" => true,
			"param_name" => "universal_color",
			"heading" => __("Color", 'universal-wp'),
            "value" => '#00bcd4', 
    		"dependency" => array(
        		"element" => "type_cont",
        		"value" => "color"
    		),
		),
		array(
			"type" => "dropdown",
            "admin_label" => true,
			"heading" => __("Slider Height", 'universal-wp'),
			"param_name" => "universal_size",
	        'value' => array(
	            __( 'FullScreen', 'universal-wp' ) => 'full',
	            __( 'Fixed', 'universal-wp' ) => 'fix',
	        ),
		),

        array(
            "type" => "textfield",
            "param_name" => "universal_size_custom",
            "value" => '500px', 
    		"dependency" => array(
        		"element" => "universal_size",
        		"value" => 'fix',
    		),
        ),
	),
    "js_view" => 'VcColumnView'
) );