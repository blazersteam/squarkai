<?php 
add_shortcode('universal_hero_kenburns', 'universal_hero_kenburns_f');
function universal_hero_kenburns_f( $atts, $content = null)
{
	extract(shortcode_atts(
		array(
            'universal_image' => null,
            'universal_size' => 'full',
            'universal_transition_in' => 'swirlRight',
            'universal_transition_out' => 'swirlLeft',
            'universal_delay' => '7000',
            'universal_duration' => '2000',
			'universal_size_custom' => '500px',
			"css" => null
		), $atts)
	);

    if ($universal_image && count($universal_image) > 0) {
          $universal_image = explode(',', $universal_image);
        } else {
          return
            '<div class="photo-none">' .
              '<p>'. __("You didn't select any image.", 'universal-wp') . '</p>' .
            '</div>';
        }

	if($universal_size == 'full'){
		$output ='<div class="intro full">';
	} else {
		$output ='<div class="intro" style="height:'.$universal_size_custom.' !important;">';
	};
		$output .='<div class="intro-body">';
			$output .=''.do_shortcode($content).'';
		$output .='</div>';
	$output .='</div>';
	$output .='
    <script>
        jQuery.noConflict()(function($){
        "use strict";

        var introHeader = $(".intro"),
            intro = $(".intro");

        buildModuleHeader(introHeader);

        $(window).resize(function() {
            var width = Math.max($(window).width(), window.innerWidth);
            buildModuleHeader(introHeader);
        });

        $(window).scroll(function() {
            effectsModuleHeader(introHeader, this);
        });

        intro.each(function(i) {
            if ($(this).attr("data-background")) {
                $(this).css("background-image", "url(" + $(this).attr("data-background") + ")");
            }
        });


        function buildModuleHeader(introHeader) {
        };
        function effectsModuleHeader(introHeader, scrollTopp) {
            if (introHeader.length > 0) {
                var homeSHeight = introHeader.height();
                var topScroll = $(document).scrollTop();
                if ((introHeader.hasClass("intro")) && ($(scrollTopp).scrollTop() <= homeSHeight)) {
                    introHeader.css("top", (topScroll * .4));
                }
                if (introHeader.hasClass("intro") && ($(scrollTopp).scrollTop() <= homeSHeight)) {
                    introHeader.css("opacity", (1 - topScroll/introHeader.height() * 1));
                }
            }
        };

        $(function() {
            $("body").vegas({
                  delay: '. esc_attr($universal_delay) .',
                  timer: false,
                  transitionDuration: '. esc_attr($universal_duration) .',
                  slides: [';
                    foreach ($universal_image as $attach_id) {
                            $thumb = wp_get_attachment_image_src($attach_id, true);
                            $thumb = $thumb[0];
                            $output .= '{src: "'. esc_url($thumb) .'"},';
                        }
                      
                $output .='],
                  transition: [ "'. esc_attr($universal_transition_in) .'", "'. esc_attr($universal_transition_out) .'" ],
                  animation: [ "kenburns" ]
              });   });
        });
    </script>';
	return $output;
};


vc_map( array(
	"name" => __("Hero KenBurns", 'universal-wp'),
	"base" => "universal_hero_kenburns",
	"category" => __('Headers', 'universal-wp'),
    "as_parent" => array('only' => 'vc_title_slider, vc_sub_title_slider, vc_mouse_slider, vc_image_slider, vc_rotate_title, vc_button_slider, vc_text_slider, vc_mailchimp_slider, vc_comingsoom_slider'),
    "content_element" => true,
    "show_settings_on_create" => true,
	"params" => array(
        array(
            "type" => "dropdown",
            "admin_label" => true,
            "heading" => __("Image Height", 'universal-wp'),
            "param_name" => "universal_size",
            'value' => array(
                __( 'FullScreen', 'universal-wp' ) => 'full',
                __( 'Fixed', 'universal-wp' ) => 'fix',
            ),
        ),
        array(
            "type" => "textfield",
            "admin_label" => true,
            "param_name" => "universal_size_custom",
            "value" => '500px', 
            "dependency" => array(
                "element" => "universal_size",
                "value" => 'fix',
            ),
        ),
        array(
            "type" => "attach_images",
            "admin_label" => true,
            "param_name" => "universal_image",
            "heading" => __("Images", 'universal-wp'),
            "value" => '',
        ),
        array(
            "type" => "textfield",
            "admin_label" => true,
            "heading" => __("Image Delay", 'universal-wp'),
            "param_name" => "universal_delay",
            "value" => '7000', 
        ),
        array(
            "type" => "textfield",
            "admin_label" => true,
            "heading" => __("Transition Duration", 'universal-wp'),
            "param_name" => "universal_duration",
            "value" => '2000', 
        ),
        array(
            "type" => "dropdown",
            "admin_label" => true,
            "heading" => __("Transition In", 'universal-wp'),
            "param_name" => "universal_transition_in",
            'value' => array(
                'fade' => 'fade',
                'blur' => 'blur',
                'burn' => 'burn',
                'flash' => 'flash',
                'negative' => 'negative',
                'slideDown' => 'slideDown',
                'slideLeft' => 'slideLeft',
                'slideRight' => 'slideRight',
                'slideUp' => 'slideUp',
                'swirlLeft' => 'swirlLeft',
                'swirlRight' => 'swirlRight',
                'zoomIn' => 'zoomIn',
                'zoomOut' => 'zoomOut',
            ),
            'std' => 'swirlRight',
        ),
        array(
            "type" => "dropdown",
            "admin_label" => true,
            "heading" => __("Transition Out", 'universal-wp'),
            "param_name" => "universal_transition_out",
            'value' => array(
                'fade' => 'fade',
                'blur' => 'blur',
                'burn' => 'burn',
                'flash' => 'flash',
                'negative' => 'negative',
                'slideDown' => 'slideDown',
                'slideLeft' => 'slideLeft',
                'slideRight' => 'slideRight',
                'slideUp' => 'slideUp',
                'swirlLeft' => 'swirlLeft',
                'swirlRight' => 'swirlRight',
                'zoomIn' => 'zoomIn',
                'zoomOut' => 'zoomOut',
            ),
            'std' => 'swirlLeft',
        ),
	),
    "js_view" => 'VcColumnView'
) );