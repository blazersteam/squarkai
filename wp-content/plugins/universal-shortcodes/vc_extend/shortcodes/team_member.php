<?php
/*TEAM  MEMBER*/
add_shortcode('vc_team_member', 'vc_team_member_f');
function vc_team_member_f($atts, $content = null) {
	extract(shortcode_atts( array(
	'image'=> get_template_directory_uri().'/assets/img/no_image.png',
	'name'=> 'James Daniels',
	'position'=>'CEO',
   'tw_url'=>'#',
   'fb_url'=>'#',
   'gp_url'=>'#',
   'inst_url'=>'',
   'drib_url'=>'',
	), $atts));
	


    if ($tw_url == ''){$tw ='';} else { $tw = '<li><a target="_blank" href="'.$tw_url.'"><i class="fa-lg fa fa-twitter"></i></a></li>';};
    if ($fb_url == ''){$fb ='';} else { $fb = '<li><a target="_blank" href="'.$fb_url.'"><i class="fa-lg fa fa-facebook"></i></a></li>';};
    if ($gp_url == ''){$gp ='';} else { $gp = '<li><a target="_blank" href="'.$gp_url.'"><i class="fa-lg fa fa-google-plus"></i></a></li>';};
    if ($inst_url == ''){$inst ='';} else { $inst = '<li><a target="_blank" href="'.$inst_url.'"><i class="fa-lg fa fa-instagram"></i></a></li>';};
    if ($drib_url == ''){$db ='';} else { $db = '<li><a target="_blank" href="'.$drib_url.'"><i class="fa-lg fa fa-dribbble"></i></a></li>';};

	 

	 $ulrs = ''.$tw.''.$fb.''.$gp.''.$inst.''.$db.'';

	 $image_done = wp_get_attachment_image($image,'full img-responsive');
	 
	 $code = '<div class="team-image">
                    '.$image_done.'
                  </div>
                  <div class="team-block">
                    <div class="about-name"><h2 class="classic">'.$name.'</h2></div>
                    <ul class="list-inline">
                    '.$ulrs.'
                    </ul>
                    <div class="about-desc"><h6>'.$position.'</h6></div>
                  </div>';

	return $code;
};


vc_map( array(
   "name" => __("Team Member",'universal-wp'),
   "base" => "vc_team_member",
   "category" => __('Universal','universal-wp'),
   "params" => array(
	  array(
         "type" => "attach_image",
         "heading" => __("Member Photo",'universal-wp'),
         "param_name" => "image",
         "admin_label" => true,
      ),
	  array(
         "type" => "textfield",
         "heading" => __("Name",'universal-wp'),
         "param_name" => "name",
         "value" => __("James Daniels",'universal-wp'),
         "admin_label" => true,
      ),
	  
	  array(
         "type" => "textfield",
         "heading" => __("Position in company",'universal-wp'),
         "param_name" => "position",
         "value" => __("CEO",'universal-wp'),
         "admin_label" => true,
      ),
	  array(
         "type" => "textfield",
         "admin_label" => true,
         "heading" => __("Twitter",'universal-wp'),
         "param_name" => "tw_url",
         "value" => __("#",'universal-wp'),
      ),
	   array(
         "type" => "textfield",
         "admin_label" => true,
         "heading" => __("Facebook",'universal-wp'),
         "param_name" => "fb_url",
         "value" => __("#",'universal-wp'),
      ),
	   array(
         "type" => "textfield",
         "admin_label" => true,
         "heading" => __("Google Plus",'universal-wp'),
         "param_name" => "gp_url",
         "value" => __("#",'universal-wp'),
      ),
	   array(
         "type" => "textfield",
         "admin_label" => true,
         "heading" => __("Instagram",'universal-wp'),
         "param_name" => "inst_url",
         "value" => __("",'universal-wp'),
      ),
	  
	   array(
         "type" => "textfield",
         "admin_label" => true,
         "heading" => __("Dribbble",'universal-wp'),
         "param_name" => "drib_url",
         "value" => __("",'universal-wp'),
      ),
   )
) );

