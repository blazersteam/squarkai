<?php
/*TESTIMONIAL  ITEM*/
add_shortcode('vc_testimonial_item', 'vc_testimonial_item_f');
function vc_testimonial_item_f( $atts, $content = null)
{
	extract(shortcode_atts(
		array(
			'id' => '',
			'white' => null,
		), $atts)
	);
	if ($white) $white = 'white';

	$post = get_post($id);
	$image = wp_get_attachment_image_src( get_post_thumbnail_id( $id ), 'wall-portfolio-squre'); 
	$title = $post->post_title;
	$content = $post->post_content;
    $output ='<div class="testimonials-item '. esc_attr($white) .'">';
    if ($image[0]!=false)   { $output .='<img src="'.$image[0].'" alt="" class="center-block">';};
      $output .='<div class="testimonials-caption">';
        $output .='<h2 class="classic">'.$title.'</h2>';
        $output .='<h5 class="no-pad">'.$content.'</h5>';
      $output .='</div>';
    $output .='</div>';

	return $output;
};


vc_map( array(
	"name" => __("Testimonial Item",'universal-wp'),
	"base" => "vc_testimonial_item",
	"category" => __('Universal','universal-wp'),
	"params" => array(
		array(
			"type" => "textfield",
			"admin_label" => true,
			"param_name" => "id",
			"heading" => __("Testimonial Item", 'universal-wp'),
			"description" => __( "Tesimonial ID", 'universal-wp' )
		),
		array(
    		"type" => "checkbox",
    		"admin_label" => true,
    		"heading" => __("White font", 'universal-wp'),
    		"param_name" => "white",
			"value" => array("Yes" => true),
		),
		
	)
) );
