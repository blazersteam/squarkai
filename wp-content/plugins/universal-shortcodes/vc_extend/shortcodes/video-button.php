<?php 
/*Video Button*/
add_shortcode('universal_video', 'universal_video_f');
function universal_video_f( $atts, $content = null)
{

	extract(shortcode_atts(
		array(
			'universal_video_text' => 'Video',
			'universal_video_text_bold' => 'Watch',
			'universal_video_sub_text' => 'A business has to be involving, it has to be fun, and it has to exercise your creative instincts. Start where you are. Use what you have. Do what you can.',
			'universal_video_link' => 'https://vimeo.com/155463374',
			"white" => null,
			"css" => null
		), $atts)
	);
	
	if ($white) $white = 'white';

	$output ='<div class="video-block '. esc_attr($white) .'">
				<a href="'. esc_url($universal_video_link) .'" class="swipebox-video"><span><i class="ion-ios-videocam-outline"></i></span></a>
            	<h2>'. esc_attr($universal_video_text_bold) .' <span class="bold">'. esc_attr($universal_video_text) .'</span></h2>
            	<p>'. esc_attr($universal_video_sub_text) .'</p>
            </div>';
	return $output;
};

/*Video Button*/
vc_map( array(
	"name" => __("Video Button",'universal-wp'),
	"base" => "universal_video",
	"category" => __('Universal','universal-wp'),
	"params" => array(
		array(
			"type" => "textfield",
			"admin_label" => true,
			"param_name" => "universal_video_text_bold",
			"heading" => __("Title", 'universal-wp'),
			"value" => 'Watch',
		),
		array(
			"type" => "textfield",
			"admin_label" => true,
			"param_name" => "universal_video_text",
			"heading" => __("Title With Bold Font", 'universal-wp'),
			"value" => 'Video',
		),

		array(
			"type" => "textarea",
			"admin_label" => true,
			"param_name" => "universal_video_sub_text",
			"heading" => __("Sub Text", 'universal-wp'),
			"value" => 'A business has to be involving, it has to be fun, and it has to exercise your creative instincts. Start where you are. Use what you have. Do what you can.',
		),
		array(
			"type" => "textarea",
			"admin_label" => true,
			"param_name" => "universal_video_link",
			"heading" => __("Link to video", 'universal-wp'),
			"value" => 'https://vimeo.com/155463374',
		),
		array(
    		"type" => "checkbox",
    		"admin_label" => true,
    		"heading" => __("White fonts", 'universal-wp'),
    		"param_name" => "white",
			"value" => array("Yes" => true),
 		),
	)
) );