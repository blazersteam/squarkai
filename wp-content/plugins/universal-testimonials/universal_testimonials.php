<?php
/**
* Plugin Name: Universal Testimonials
* Plugin URI: http://themeforest.net/user/DankovThemes
* Description: Testimonials Plugin
* Version: 1.0.0
* Author: DankovThemes
* Author URI: http://themeforest.net/user/DankovThemes
* License: 
*/

//Create Post Formats
add_action( 'init', 'universal_testimonials' );
function universal_testimonials() {
	register_post_type( 'testimonials',
		array(
			'labels' => array(
				'name' => __( 'Testimonials', 'universal' ),
				'singular_name' => __( 'Testimonials', 'universal' ),
				'new_item' => __( 'Add New testimonial', 'universal' ),
				'add_new_item' => __( 'Add New testimonial', 'universal' )
			),
			'menu_icon'           => 'dashicons-format-status',
			'public' => true,
			'has_archive' => false,
			'supports' => array( 'comments', 'editor', 'excerpt', 'thumbnail', 'title' ),
			'capability_type' => 'post',
			'show_ui' => true,
			'publicly_queryable' => true,
			'rewrite' => array('slug' => 'testimonials'),
		)
	);
}

?>