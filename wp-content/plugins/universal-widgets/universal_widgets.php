<?php
/**
* Plugin Name: Universal Widgets
* Plugin URI: http://themeforest.net/user/DankovThemes
* Description: Widgets Plugin
* Version: 1.0.2
* Author: DankovThemes
* Author URI: http://themeforest.net/user/DankovThemes
* License: 
*/

include(plugin_dir_path( __FILE__ ).'widgets/universal_popular_posts.php');
include(plugin_dir_path( __FILE__ ).'widgets/universal_latest_posts.php');
include(plugin_dir_path( __FILE__ ).'widgets/universal_recent_posts_comments.php');
include(plugin_dir_path( __FILE__ ).'widgets/universal_soc_link.php');
include(plugin_dir_path( __FILE__ ).'widgets/universal_counter.php');


/**
 * Add function to widgets_init that'll load our widget.
 */

add_action('widgets_init', 'universal_get_in_touch_widget');

function universal_get_in_touch_widget() {
	register_widget('Universal_Get_In_Touch_Widget');
}


/**
 * Widget class.
 * This class handles everything that needs to be handled with the widget:
 * the settings, form, display, and update. 
 *
 */
class Universal_Get_In_Touch_Widget extends WP_Widget {

	/**
	 * Widget setup.
	 */		

	function __construct() {
		parent::__construct(
			'universal-get-in-touch-widget',
			__( 'Universal: Get in touch', 'universal' ),
			array(
				'classname' => 'universal-get-in-touch-widget',
				'description' => esc_html__( 'Get in touch with clients', 'universal' ),
				'customize_selective_refresh' => true
			)
		);
	}



	/*-----------------------------------------------------------------------------------*/
	/*	Display Widget
	/*-----------------------------------------------------------------------------------*/
	
	function widget( $args, $instance ) {
		extract( $args );
		
		// Our variables from the widget settings
		$title = apply_filters('widget_title', $instance['title'] );
		$adress = $instance['adress'];
		$phone = $instance['phone'];
		$fax = $instance['fax'];
		$skype = $instance['skype'];
		$email = $instance['email'];
		$weekend = $instance['weekend'];

		$time_id = rand();

		/* Before widget (defined by themes). */
		// Before widget (defined by theme functions file)
	echo $before_widget;
	// Display the widget title if one was input
	if ( $title )
		echo $before_title . $title . $after_title;
		?>

		<div class="forbetterweb_about_widget">
				<ul class="contact-footer contact-composer">
                  	<?php if($adress != false) {?><li><span><?php echo  $instance['adress']?></span></li><?php } ?>
                  	<?php if($phone != false) {?><li><span><?php echo  $instance['phone']?></span></li><?php } ?>
                  	<?php if($fax != false) {?><li><span><?php echo  $instance['fax']?></span></li><?php } ?>
                  	<?php if($skype != false) {?><li><span><?php echo  $instance['skype']?></span></li><?php } ?>
                  	<?php if($email != false) {?><li><span><?php echo  $instance['email']?></span></li><?php } ?>
                  	<?php if($weekend != false) {?><li><span><?php echo  $instance['weekend']?></span></li><?php } ?>
                </ul>   
        </div>

		<?php

		// After widget (defined by theme functions file)
		echo $after_widget;
	}


/*-----------------------------------------------------------------------------------*/
/*	Update Widget
/*-----------------------------------------------------------------------------------*/
	
function update( $new_instance, $old_instance ) {
	$instance = $old_instance;

	// Strip tags to remove HTML (important for text inputs)
	$instance['title'] = strip_tags( $new_instance['title'] );
	// Stripslashes for html inputs
	$instance['adress'] = $new_instance['adress'];
	$instance['phone'] = $new_instance['phone'];
	$instance['fax'] = $new_instance['fax'];
	$instance['skype'] = $new_instance['skype'];
	$instance['email'] = $new_instance['email'];
	$instance['weekend'] = $new_instance['weekend'];

	return $instance;
}


/*-----------------------------------------------------------------------------------*/
/*	Widget Settings (Displays the widget settings controls on the widget panel)
/*-----------------------------------------------------------------------------------*/
	 
function form( $instance ) {

	// Set up some default widget settings
	$defaults = array(	'title'		=> __( 'Our Contacts' , 'universal' ),
						'adress'	=> __( 'Adress: 455 Martinson, Los Angeles' , 'universal' ),
						'fax'		=> '',
						'skype'		=> '',
						'phone'		=> __( 'Phone: 8 (043) 567 - 89 - 30' , 'universal' ),
						'email'		=> __( 'E-mail: support@email.com' , 'universal' ),
						'weekend'	=> __( 'Weekend: from 9 am to 6 pm' , 'universal' ),
					);
	
	$instance = wp_parse_args((array) $instance, $defaults);
	?>


	<!-- Widget Title: Text Input -->
	<p>
		<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e('Title:', 'universal') ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo stripslashes(htmlspecialchars(( $instance['title'] ), ENT_QUOTES)); ?>" />
	</p>
    <p>
		<label for="<?php echo $this->get_field_id( 'adress' ); ?>"><?php _e('Adress:', 'universal') ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id( 'adress' ); ?>" name="<?php echo $this->get_field_name( 'adress' ); ?>" value="<?php echo stripslashes(htmlspecialchars(( $instance['adress'] ), ENT_QUOTES)); ?>" />
	</p>
    <p>
		<label for="<?php echo $this->get_field_id( 'phone' ); ?>"><?php _e('Phone:', 'universal') ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id( 'phone' ); ?>" name="<?php echo $this->get_field_name( 'phone' ); ?>" value="<?php echo stripslashes(htmlspecialchars(( $instance['phone'] ), ENT_QUOTES)); ?>" />
	</p>
    <p>
		<label for="<?php echo $this->get_field_id( 'fax' ); ?>"><?php _e('Fax:', 'universal') ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id( 'fax' ); ?>" name="<?php echo $this->get_field_name( 'fax' ); ?>" value="<?php echo stripslashes(htmlspecialchars(( $instance['fax'] ), ENT_QUOTES)); ?>" />
	</p>
    <p>
		<label for="<?php echo $this->get_field_id( 'skype' ); ?>"><?php _e('Skype:', 'universal') ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id( 'skype' ); ?>" name="<?php echo $this->get_field_name( 'skype' ); ?>" value="<?php echo stripslashes(htmlspecialchars(( $instance['skype'] ), ENT_QUOTES)); ?>" />
	</p>
    <p>
		<label for="<?php echo $this->get_field_id( 'email' ); ?>"><?php _e('E-mail:', 'universal') ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id( 'email' ); ?>" name="<?php echo $this->get_field_name( 'email' ); ?>" value="<?php echo stripslashes(htmlspecialchars(( $instance['email'] ), ENT_QUOTES)); ?>" />
	</p>
    <p>
		<label for="<?php echo $this->get_field_id( 'weekend' ); ?>"><?php _e('Open/Close:', 'universal') ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id( 'weekend' ); ?>" name="<?php echo $this->get_field_name( 'weekend' ); ?>" value="<?php echo stripslashes(htmlspecialchars(( $instance['weekend'] ), ENT_QUOTES)); ?>" />
	</p>

	<?php
	}
}
?>