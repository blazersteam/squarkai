  <nav class="navbar navbar-wrap navbar-custom navbar-fixed-top menu-wrap">
      <div class="full">
    <!-- <div class="container full"> -->
        <div class="row header-top-bar">
    <div class="col-lg-5 pull-right">
<div class="top-menu visible-lg visible-md visible-sm">
              <ul>
                  <li class="sub-menu">
                  <div class="search-icon-header">
                    <a href="#"><i class="fa fa-search fa-lg"></i></a>
                      <div class="black-search-block">
                        <div class="black-search-table">
                          <div class="black-search-table-cell">
                            <div>
                              <form role="search" method="get" id="searchform" action="<?php echo esc_url(home_url('/')); ?>" >
                                <input type="text" class="focus-input" placeholder="Enter Keyword" value="<?php echo get_search_query() ?>" name="s" id="s" />
                                <input type="submit" id="searchsubmit" value="" />
                              </form>
                            </div>
                          </div>
                        </div>
                        <div class="close-black-block"><a href="#"><i class="ion-ios-close-empty"></i></a></div>
                      </div>
                    </div>
                </li>
                <li class="menu-divider sub-menu">&nbsp;</li>
                <li class="sub-menu visible-lg visible-md visible-sm">
                    <div class="search-icon-header"><a href="#" style="font-family: sans-serif;">+1 (888) 747-8471</a></div></li>
                <li class="sub-menu visible-xs"><a href="#"><i class="fa fa-phone fa-lg"></i></a></li>
                   <li class="menu-divider sub-menu">&nbsp;</li>
                  <li class="sub-menu"><a href="/contact-us/">Contact Us</a></li>
                   <li class="menu-divider sub-menu">&nbsp;</li>
                  <li class="sub-menu"><a href="https://predictor.vizadata.com/login">Login</a></li>
              </ul>
          </div>
    </div>
</div>

        <div class="row">
          <div class="col-lg-2 col-md-4 col-sm-6 col-xs-6">
              <div class="logo">
                <a href="<?php echo esc_url(home_url('/')); ?>" title="<?php bloginfo( 'name' ); ?>"><img src="<?php echo esc_url(get_theme_mod('universal_logo_light', get_template_directory_uri() . '/assets/images/logo.png')); ?>" style="height: <?php echo esc_attr(get_theme_mod('universal_logo_height_light', '28px')); ?>" class="logowhite" alt="<?php the_title_attribute(); ?>" >
                  <img src="<?php echo esc_url(get_theme_mod('universal_logo_dark', get_template_directory_uri() . '/assets/images/logo-dark.png')); ?>" style="height: <?php echo esc_attr(get_theme_mod('universal_logo_height_dark', '20px')); ?>" class="logodark" alt="<?php the_title_attribute(); ?>" >
                </a>
              </div>
          </div>
          <div class="col-lg-10 col-md-8 col-sm-6 col-xs-6 pull-right">
          <?php if(get_theme_mod('universal_menu_select', 'standard') == 'standard')  { ?> 
          
          <div class="top-menu" style="display:none">
              <ul>
                  <li class="sub-menu">Search</li>
                   <li class="menu-divider sub-menu">&nbsp;</li>
                  <li class="sub-menu">Contact Us</li>
                   <li class="menu-divider sub-menu">&nbsp;</li>
                  <li class="sub-menu">Login</li>
              </ul>
          </div>
          
            <div class="menu-center">
              <div class="menu-responsive desktop">
                <div class="collapse navbar-collapse navbar-main-collapse pull-left responsive-menu">
                        <?php wp_nav_menu( array(
                          'theme_location' => 'menu',
                          'container' => false,
                          'menu_class' => 'nav navbar-nav',
                          'sort_column' => 'menu_order',
                          'walker' => new Universal_My_Walker_Nav_Menu(),
                          'fallback_cb' => 'universal_MenuFallback'
                        )); ?> 
                </div>
              </div>
              <div class="menu-responsive mobile">
                  <div class="eds_menu" style="display:none">
                  <a  href="/plan/" style="font-size: 10px;">Buy Now</a>
                  <?php
                  do_action('wp_update_nav_menu');
                  ?>
                  </div>
                <div class="burger_universal_normal_holder" style="display:inline-flex;">
                    <div class="btn-ct mob-disp">
                    <a  href="/plan/" style="font-size: 10px;">Buy Now</a>
                    </div>
                    <a href="#" class="nav-icon3" id="open-button"><span></span><span></span><span></span><span></span><span></span><span></span></a></div>
                  <div class="burger_universal_menu_overlay_normal">
                    <div class="burger_universal_menu_vertical">
                      <?php 
                      /*wp_nav_menu( array(
                        'theme_location' => 'menu',
                        'menu_class' => 'burger_universal_main_menu',
                        'depth' => 3,
                      )); */
                      
                      
                      wp_nav_menu( array( 
    'theme_location' => 'my-custom-menu', 
     'menu_class' => 'burger_universal_main_menu',
    'container_class' => 'custom-menu-class' ) ); 
                      ?>
							<div class="btn-ct" style="width: 200px;margin: 10px auto;">
                    	<a href="https://vizadata.onfastspring.com/">Buy Now</a>
                    </div>
                  <div class="btn-ct" style="width: 200px;margin: 10px auto;">
                    <a href="/free-trial/">Free Trial</a>
                    </div>
                    <div class="btn-ct" style="width: 200px;margin: 10px auto;">
                    <a data-toggle="modal" data-target="#myModal" href="#">Request Demo</a>
                    </div>
                    </div>
                  </div>
              </div>
              <?php  if(get_theme_mod('universal_menu_extras','enable') == true)  { ?>
                <ul class="cart_search_block">
                    
                <!-- <li class="menu-divider visible-lg">&nbsp;</li> -->
            
                <?php if (class_exists( 'WooCommerce' )) {?> 
                  <li>
                    <div class="universal_woo_cart">
                      <div class="universal_head_holder_inner">
                        <div class="universal_head_cart">
                            <a class="" href="<?php echo WC()->cart->get_cart_url(); ?>"><i class="fa fa-shopping-bag fa-lg"></i> <span class="universal_cart_icon"><?php echo sprintf (_n( '%d', '%d', WC()->cart->cart_contents_count, 'universal-wp' ), WC()->cart->cart_contents_count ); ?></span></a>
                        </div>
                      </div>
                      <div class="universal_cart_widget">
                        <?php the_widget( 'WC_Widget_Cart', 'title=' );?>
                      </div>
                    </div>
                  </li>
                  <?php } ?>
                  <li style="display:none;">
                  <div class="search-icon-header visible-lg">
                    <a href="#"><i class="fa fa-search fa-lg"></i></a>
                      <div class="black-search-block">
                        <div class="black-search-table">
                          <div class="black-search-table-cell">
                            <div>
                              <form role="search" method="get" id="searchform" action="<?php echo esc_url(home_url('/')); ?>" >
                                <input type="text" class="focus-input" placeholder="Enter Keyword" value="<?php echo get_search_query() ?>" name="s" id="s" />
                                <input type="submit" id="searchsubmit" value="" />
                              </form>
                            </div>
                          </div>
                        </div>
                        <div class="close-black-block"><a href="#"><i class="ion-ios-close-empty"></i></a></div>
                      </div>
                    </div>
                </li>
               
                <li>
                  <div class="btn-ct visible-lg">
                    <a href="https://vizadata.onfastspring.com/">Buy Now</a>
                    </div>
                </li>
                 <li>
                  <div class="btn-ct visible-lg">
                    <a href="/free-trial/">Free Trial</a>
                    </div>
                </li>
                <li>
                  <div class="btn-ct visible-lg">
                    <a data-toggle="modal" data-target="#myModal" href="#">Request Demo</a>
                    </div>
                </li>
                
              </ul>
            <?php }; ?>
          </div>
          <?php } else { ?>
            <div class="menu-center">
              <div class="menu-responsive desktop">
                <div class="collapse navbar-collapse navbar-main-collapse pull-left responsive-menu">
                        <?php wp_nav_menu( array(
                          'theme_location' => 'onepage-menu',
                          'container' => false,
                          'menu_class' => 'nav navbar-nav share-class',
                          'menu_id' => 'menu-onepage',
                          'sort_column' => 'menu_order',
                          'walker' => new Universal_My_Walker_Nav_Menu(),
                          'fallback_cb' => 'universal_MenuFallback'
                        )); ?> 
                </div>
              </div>
              <div class="menu-responsive mobile">
                  
                <div class="burger_universal_normal_holder"><a href="#" class="nav-icon3" id="open-button"><span></span><span></span><span></span><span></span><span></span><span></span></a></div>
                  <div class="burger_universal_menu_overlay_normal">
                    <div class="burger_universal_menu_vertical">
                      <?php wp_nav_menu( array(
                        'theme_location' => 'onepage-menu',
                        'menu_class' => 'burger_universal_main_menu share-class',
                        'menu_id' => 'menu-onepage',
                        'depth' => 2,
                      )); ?>
						
                  
                    </div>
                  </div>
              </div>
              <?php  if(get_theme_mod('universal_menu_extras','enable') == true)  { ?>
                <ul class="cart_search_block">
                <li class="menu-divider visible-lg">&nbsp;</li>
                <?php if (class_exists( 'WooCommerce' )) {?> 
                  <li>
                    <div class="universal_woo_cart">
                      <div class="universal_head_holder_inner">
                        <div class="universal_head_cart">
                            <a class="" href="<?php echo WC()->cart->get_cart_url(); ?>"><i class="fa fa-shopping-bag fa-lg"></i> <span class="universal_cart_icon"><?php echo sprintf (_n( '%d', '%d', WC()->cart->cart_contents_count, 'universal-wp' ), WC()->cart->cart_contents_count ); ?></span></a>
                        </div>
                      </div>
                      <div class="universal_cart_widget">
                        <?php the_widget( 'WC_Widget_Cart', 'title=' );?>
                      </div>
                    </div>
                  </li>
                  <?php } ?>
                  <li>
                  <div class="search-icon-header">
                    <a href="#"><i class="fa fa-search fa-lg"></i></a>
                      <div class="black-search-block">
                        <div class="black-search-table">
                          <div class="black-search-table-cell">
                            <div>
                              <form role="search" method="get" id="searchform" action="<?php echo esc_url(home_url('/')); ?>" >
                                <input type="text" class="focus-input" placeholder="Enter Keyword" value="<?php echo get_search_query() ?>" name="s" id="s" />
                                <input type="submit" id="searchsubmit" value="" />
                              </form>
                            </div>
                          </div>
                        </div>
                        <div class="close-black-block"><a href="#"><i class="ion-ios-close-empty"></i></a></div>
                      </div>
                    </div>
                </li>
              </ul>
            <?php }; ?>
          </div>
          <?php } ?>
        </div> 
        </div> 
      </div>
    </nav>