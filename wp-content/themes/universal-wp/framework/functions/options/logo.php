<?php

    $fields[] = array(
        'type'        => 'color',
        'settings'    => 'header_color_image',
        'label'       => __( 'Header Overlay', 'universal-wp' ),
        'description' => esc_attr__( 'You can control color of overlay top image.', 'universal-wp' ),
        'section'     => 'universal_logo_section',
        'default'     => 'rgba(7,9,21,0.5)',
        'choices'     => array(
            'alpha' => true,
        )
    );

    $fields[] = array(
        'type'        => 'color',
        'settings'    => 'header_color_image_title',
        'description' => esc_attr__( 'Also, you can change color of text inside area.', 'universal-wp' ),
        'section'     => 'universal_logo_section',
        'default'        => '#ffffff',
        'active_callback'  => array(
            array(
                'setting'  => 'header_color_image',
                'operator' => '!==',
                'value'    => '',
            ),
        ),
    );

     $fields[] = array(
        'type'        => 'select',
        'settings'    => 'universal_menu_select',
        'label'       => __( 'Type of menu', 'universal-wp' ),
        'section'     => 'universal_logo_section',
        'default'     => 'standard',
        'choices'     => array(
            'standard'   => esc_attr__( 'Standard', 'universal-wp' ),
            'onepage'   => esc_attr__( 'Onepage', 'universal-wp' )
        ),
    );

     $fields[] = array(
        'type'        => 'toggle',
        'settings'    => 'universal_menu_extras',
        'label'       => __( 'Menu Search, Cart', 'universal-wp' ),
        'section'     => 'universal_logo_section',
        'default'     => '1',
        'priority'    => 10,
        'choices'     => array(
            'on'  => esc_attr__( 'Show', 'universal-wp' ),
            'off' => esc_attr__( 'Hide', 'universal-wp' ),
        ),
    );

     $fields[] = array(
        'type'        => 'image',
        'settings'     => 'universal_logo_light',
        'description' => __( 'Add logo', 'universal-wp' ),
        'section'     => 'universal_logo_section',
        'default'     => get_template_directory_uri() . '/assets/images/logo.png',
    ); 

    $fields[] = array(
        'type'        => 'text',
        'settings'    => 'universal_logo_height_light',
        'label'       => __( 'Light Logo Height', 'universal-wp' ),
        'section'     => 'universal_logo_section',
        'default'     => '28px',
    );

     $fields[] = array(
        'type'        => 'image',
        'settings'     => 'universal_logo_dark',
        'description' => __( 'Add dark logo', 'universal-wp' ),
        'section'     => 'universal_logo_section',
        'default'     => get_template_directory_uri() . '/assets/images/logo-dark.png',
    ); 

    $fields[] = array(
        'type'        => 'text',
        'settings'    => 'universal_logo_height_dark',
        'label'       => __( 'Dark Logo Height', 'universal-wp' ),
        'section'     => 'universal_logo_section',
        'default'     => '20px',
    );


     $fields[] = array(
        'type'        => 'image',
        'settings'     => 'universal_logo_favicon',
        'label'       => __( 'Favicon', 'universal-wp'),
        'description' => __( 'Image 144x144 in png', 'universal-wp' ),
        'section'     => 'universal_logo_section',
        'default'     => get_template_directory_uri() . '/assets/images/favicon.png',
    );

?>