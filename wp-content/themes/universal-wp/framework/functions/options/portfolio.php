<?php

    $fields[] = array(
        'type'        => 'image',
        'settings'     => 'universal_single_portfolio_image',
        'label' =>    esc_html__( 'Static Top Image', 'universal-wp' ),
        'section'     => 'universal_portfolio',
        'priority'    => 10,
    );

    $fields[] = array(
        'type'        => 'text',
        'settings'     => 'universal_title_portfolio',
        'label'       => __( 'Static Title', 'universal-wp' ),
        'description' => __( 'You can set up static title for all single portfolio.', 'universal-wp' ),
        'section'     => 'universal_portfolio',
        'priority'    => 10,
    );
    
    $fields[] = array(
        'type'        => 'text',
        'settings'     => 'universal_subtitle_portfolio',
        'label'       => __( 'Name in breadcrumbs', 'universal-wp' ),
        'section'     => 'universal_portfolio',
        'default'     => 'Single Project',
        'priority'    => 10,
        'active_callback'  => array(
            array(
                'setting'  => 'universal_breadcrumbs',
                'operator' => '==',
                'value'    => true,
            ),
        ),
    );

    $fields[] = array(
        'type'        => 'toggle',
        'settings'    => 'universal_single_portfolio_vc',
        'label'       => __( 'WPBakery Page Builder', 'universal-wp' ),
        'description' => __( 'You can use page builder for single portfolio page with a lot of features. Also you need to enable Portfolio in Post Type: <a href="https://dankov-themes.com/extra/single_portfolio_wpb.png" target="_blank">here</a>', 'universal-wp' ),
        'section'     => 'universal_portfolio',
        'priority'    => 10,
        'choices'     => array(
            'on'  => esc_attr__( 'Enable', 'universal-wp' ),
            'off' => esc_attr__( 'Disable', 'universal-wp' ),
        ),
    );

    $fields[] = array(
        'type'        => 'toggle',
        'settings'    => 'universal_additional',
        'label'       => __( 'Additional Fields', 'universal-wp' ),
        'section'     => 'universal_portfolio',
        'default'     => 'on',
        'priority'    => 10,
        'choices'     => array(
            'on'  => esc_attr__( 'Show', 'universal-wp' ),
            'off' => esc_attr__( 'Hide', 'universal-wp' ),
        ),

    );

    $fields[] = array(
        'type'        => 'text',
        'settings'     => 'universal_link_portfolio',
        'label'       => __( 'Link to Works', 'universal-wp' ),
        'description' => __( 'Add link to "All Works"', 'universal-wp' ),
        'section'     => 'universal_portfolio',
        'default'     => home_url('/portfolio'),
        'priority'    => 10,
    );



     