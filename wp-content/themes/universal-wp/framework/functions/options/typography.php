<?php


     $fields[] = array(
        'type'        => 'typography',
        'settings'    => 'body_typography',
        'label'       => esc_attr__( 'Body Typography', 'universal-wp' ),
        'section'     => 'universal_typography',
        'default'     => array(
            'font-family'    => 'Raleway',
            'variant'        => '400',
            'font-size'      => '15px',
            'line-height'    => '1.8',
            'letter-spacing' => '0.03em',
            'color'          => '#444',
            'text-transform' => 'none',
        ),
        'output'      => array(
            array(
                'element' => 'body',
            ),
        ),
        'choices' => array(
            'fonts' => array(
                'google'   => array( 'popularity', 100 ),
                    'standard' => array(
                    'Georgia,Times,"Times New Roman",serif',
                    'Roboto, Oxygen-Sans, Ubuntu, Cantarell, "Helvetica Neue", sans-serif',
                ),
            ),
        ),
    );

     $fields[] = array(
        'type'        => 'typography',
        'settings'    => 'h1_typography',
        'label'       => esc_attr__( 'H1 Typography', 'universal-wp' ),
        'section'     => 'universal_typography',
        'default'     => array(
            'font-family'    => 'Raleway',
            'variant'        => '800',
            'font-size'      => '36px',
            'line-height'    => '1.7',
            'letter-spacing' => '0.1em',
            'color'          => '',
            'text-transform' => 'uppercase',
        ),
        'output'      => array(
            array(
                'element' => 'h1',
            ),
        ),
        'choices' => array(
            'fonts' => array(
                'google'   => array( 'popularity', 100 ),
                    'standard' => array(
                    'Georgia,Times,"Times New Roman",serif',
                    'Roboto, Oxygen-Sans, Ubuntu, Cantarell, "Helvetica Neue", sans-serif',
                ),
            ),
        ),
    );
     
     $fields[] = array(
        'type'        => 'typography',
        'settings'    => 'h2_typography',
        'label'       => esc_attr__( 'H2 Typography', 'universal-wp' ),
        'section'     => 'universal_typography',
        'default'     => array(
            'font-family'    => 'Raleway',
            'variant'        => '800',
            'font-size'      => '30px',
            'line-height'    => '1.7',
            'letter-spacing' => '0.1em',
            'color'          => '',
            'text-transform' => 'uppercase',
        ),
        'output'      => array(
            array(
                'element' => 'h2',
            ),
        ),
        'choices' => array(
            'fonts' => array(
                'google'   => array( 'popularity', 100 ),
                    'standard' => array(
                    'Georgia,Times,"Times New Roman",serif',
                    'Roboto, Oxygen-Sans, Ubuntu, Cantarell, "Helvetica Neue", sans-serif',
                ),
            ),
        ),
    );

     $fields[] = array(
        'type'        => 'typography',
        'settings'    => 'h3_typography',
        'label'       => esc_attr__( 'H3 Typography', 'universal-wp' ),
        'section'     => 'universal_typography',
        'default'     => array(
            'font-family'    => 'Raleway',
            'variant'        => '800',
            'font-size'      => '24px',
            'line-height'    => '1.7',
            'letter-spacing' => '0.1em',
            'color'          => '',
            'text-transform' => 'uppercase',
        ),
        'output'      => array(
            array(
                'element' => 'h3',
            ),
        ),
        'choices' => array(
            'fonts' => array(
                'google'   => array( 'popularity', 100 ),
                    'standard' => array(
                    'Georgia,Times,"Times New Roman",serif',
                    'Roboto, Oxygen-Sans, Ubuntu, Cantarell, "Helvetica Neue", sans-serif',
                ),
            ),
        ),
    );

     $fields[] = array(
        'type'        => 'typography',
        'settings'    => 'h4_typography',
        'label'       => esc_attr__( 'H4 Typography', 'universal-wp' ),
        'section'     => 'universal_typography',
        'default'     => array(
            'font-family'    => 'Raleway',
            'variant'        => '800',
            'font-size'      => '18px',
            'line-height'    => '1.7',
            'letter-spacing' => '0.1em',
            'color'          => '',
            'text-transform' => 'uppercase',
        ),
        'output'      => array(
            array(
                'element' => 'h4',
            ),
        ),
        'choices' => array(
            'fonts' => array(
                'google'   => array( 'popularity', 100 ),
                    'standard' => array(
                    'Georgia,Times,"Times New Roman",serif',
                    'Roboto, Oxygen-Sans, Ubuntu, Cantarell, "Helvetica Neue", sans-serif',
                ),
            ),
        ),
    );

     $fields[] = array(
        'type'        => 'typography',
        'settings'    => 'h5_typography',
        'label'       => esc_attr__( 'H5 Typography', 'universal-wp' ),
        'section'     => 'universal_typography',
        'default'     => array(
            'font-family'    => 'Raleway',
            'variant'        => '800',
            'font-size'      => '14px',
            'line-height'    => '1.7',
            'letter-spacing' => '0.1em',
            'color'          => '',
            'text-transform' => 'uppercase',
        ),
        'output'      => array(
            array(
                'element' => 'h5',
            ),
        ),
        'choices' => array(
            'fonts' => array(
                'google'   => array( 'popularity', 100 ),
                    'standard' => array(
                    'Georgia,Times,"Times New Roman",serif',
                    'Roboto, Oxygen-Sans, Ubuntu, Cantarell, "Helvetica Neue", sans-serif',
                ),
            ),
        ),
    );

     $fields[] = array(
        'type'        => 'typography',
        'settings'    => 'h6_typography',
        'label'       => esc_attr__( 'H6 Typography', 'universal-wp' ),
        'section'     => 'universal_typography',
        'default'     => array(
            'font-family'    => 'Raleway',
            'variant'        => '800',
            'font-size'      => '12px',
            'line-height'    => '1.7',
            'letter-spacing' => '0.1em',
            'color'          => '',
            'text-transform' => 'uppercase',
        ),
        'output'      => array(
            array(
                'element' => 'h6',
            ),
        ),
        'choices' => array(
            'fonts' => array(
                'google'   => array( 'popularity', 100 ),
                    'standard' => array(
                    'Georgia,Times,"Times New Roman",serif',
                    'Roboto, Oxygen-Sans, Ubuntu, Cantarell, "Helvetica Neue", sans-serif',
                ),
            ),
        ),
    );

     $fields[] = array(
        'type'        => 'typography',
        'settings'    => 'signature_typography',
        'label'       => esc_attr__( 'Signature Typography', 'universal-wp' ),
        'section'     => 'universal_typography',
        'default'     => array(
            'font-family'    => 'Caveat',
            'variant'        => '400',
            'font-size'      => '30px',
            'line-height'    => '1.7',
            'letter-spacing' => '0',
            'color'          => '',
            'text-transform' => 'capitalize',
        ),
        'output'      => array(
            array(
                'element' => '.signature_vc, blockquote cite a, .classic, .classic2',
            ),
        ),
        'choices' => array(
            'fonts' => array(
                'google'   => array( 'popularity', 100 ),
            ),
        ),
    );