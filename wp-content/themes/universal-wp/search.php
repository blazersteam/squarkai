<?php get_header(); ?>
   <?php $layout_value_index = get_theme_mod( 'universal_post_type' ); ?>
        <?php $layout_value = get_theme_mod( 'universal_sidebars', 'sidebar-right' ); ?>
            <?php if ($layout_value == 'sidebar-left'): ?>    
            <section class="section-small">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8 col col-sm-12 sidebar-left">
                            <div class="row">
                                <div class="col-md-12 news-block-text">
                                    <div class="pull-left"><h4><?php esc_html_e('Our Latest News', 'universal-wp' ) ?></h4></div>
                                </div>
                            </div>
                            <?php get_template_part( 'framework/content/search');?>
                        </div>
                        <?php //get_sidebar('shop'); ?>
                        <?php //dynamic_sidebar('shop'); ?>
                        <!-- Add custom sidebar-->

                    </div>                
                </div>
                <?php the_posts_pagination( array('prev_text' => __('&laquo;'), 'next_text'    => __('&raquo;'))) ?>
            </section>
            <?php elseif ($layout_value == 'sidebar-right'): ?>    
            <section class="section-small">
                <div class="container" style="margin-bottom:100px;">
                    <div class="row" id="glossary">
                        <div class="col-md-8 col col-sm-12">
                            <div class="row">
                            <div class="col-md-12 news-block-text">
                                    <div class="pull-left"><h4><?php esc_html_e('Our Latest News', 'universal-wp' ) ?></h4></div>
                                </div>
                            </div>
                            <?php get_template_part( 'framework/content/search');?>
                        </div>
                         <div class="col-md-4 col-sm-12" id="list-posttitle">
							 <?php dynamic_sidebar('shop'); ?>
							<?php
								// the query
								$all_posts = new WP_Query( array( 'post_type' => 'post', 'category_name'=> 'glossary', 'post_status' => 'publish', 'posts_per_page' => -1 ) );

								if ( $all_posts->have_posts() ) :
								?>

								  <ul>
									<?php while ( $all_posts->have_posts() ) : $all_posts->the_post(); ?>
									  <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
									<?php endwhile; ?>
								  </ul>

								<?php else : ?>
								  <p><?php _e( 'Sorry, no posts were found.' ); ?></p>
								<?php endif; ?>

								<?php wp_reset_postdata(); ?>
						</div>
                    </div>
                </div>                          
                <?php the_posts_pagination( array('prev_text' => __('&laquo;'), 'next_text'    => __('&raquo;'))) ?>
            </section>
            <?php else: ?>
                <?php if ($layout_value_index == 'masonry') {?>
                    <section class="section-small">
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 no-sidebar">
                                    <div class="row">
                                        <div class="col-md-12 news-block-text">
                                            <div class="pull-left"><h4><?php esc_html_e('Our Latest News', 'universal-wp' ) ?></h4></div>
                                        </div>
                                    </div>
                                    <?php get_template_part( 'framework/content/search');?>
                                </div> 
                            </div> 
                        </div>
                        <?php the_posts_pagination( array('prev_text' => __('&laquo;'), 'next_text'    => __('&raquo;'))) ?>
                    </section>
            <?php } else { ?>
            <section class="section-small">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 no-sidebar">
                            <div class="row">
                                <div class="col-md-12 news-block-text">
                                    <div class="pull-left"><h4><?php esc_html_e('Our Latest News', 'universal-wp' ) ?></h4></div>
                                </div>
                            </div>
                            <?php get_template_part( 'framework/content/search');?>
                        </div> 
                    </div> 
                </div>
                <?php the_posts_pagination( array('prev_text' => __('&laquo;'), 'next_text'    => __('&raquo;'))) ?>
            </section>
        <?php } ?>
    <?php endif; ?>
<?php get_footer(); ?>

